# -*- coding: utf-8 -*-
"""
Created on Wed Oct 12 11:47:28 2016

@author: emil
"""

import os
import re
from parse import parse

fn_bad = "LHe_2_00K_280mW_2000msdecay_20msdrift_50mms_35fspulses_6imgpulses_14msecexp44.SPE"
fn_good = "LHe_2_00K_280mW_2000msdecay_20msdrift_50mms_35fspulses_6imgpulses_14msecexp_36.SPE"

def check(fn):
    tok = list(re.split("[_.]", fn))
    OKp = re.match("^\d+$", tok[-2])
    if not OKp:
        print(fn+" not matched!")
        exp_time, idx = parse("{}msecexp{}", tok[-2])
        newname = fn.replace(tok[-2], "{}msecexp_{}".format(exp_time, idx))
        print("new name: "+newname)
        os.rename(fn, newname)

def check_dir(dirname):
    from glob import glob
    files = glob(os.path.join(dirname, "LHe*.SPE"))
    for fn in files:
        check(fn)

if __name__ == '__main__':
    check_dir('data/T200/50mms/images/500ms')