#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as optim
from scipy.stats import gaussian_kde, kurtosis
import data_util
import data_dirs
from data_util import vinc_delta
import os.path as path
import os
from scipy.special import erf

import statsmodels.api as sm

import re

def make_basename(dumpname):
    dumpname0 = path.split(dumpname)[-1]
    toks = re.split('_', dumpname0)
    
    if len(toks) == 6:
        basename = toks[1] + '_' + toks[2] + '_' + toks[3]
    elif len(toks) == 7:
        basename = toks[1] + '_' + toks[2] + '_' + toks[3] + '_' + toks[4]
    basename = 'ekurt_' + basename + '.dat'
    print('basename=', basename)
    return basename

plt.close('all')

f, ax = plt.subplots(1,1)

for T in [1.45, 1.65, 1.85, 2.0, 2.15]:
    #get the data dumps
    files, _, _ = data_util.get_files('vinc_dumps', T, 300, 4000, force_pattern='DUMP*')
    files.sort(key=vinc_delta)
    
    deltas = []
    ks = []
    for fn in files:
        print(fn)
        d = np.loadtxt(fn)
        dv = d[:,2]
        deltas.append(vinc_delta(fn))
        ks.append(kurtosis(dv))
        
    ax.plot(deltas, ks, '-o', label=str(T))
#    pl = ax.plot(deltas[1:], ks[1:], '-o')
#    ax.axhline(ks[0], label = str(T), color=pl[-1].get_color())
    out_dir = path.join(data_dirs.get_base_data_dir(which='processed'),
                        'kurtosis',
                        'T{:.0f}'.format(100*T))
    os.makedirs(out_dir, exist_ok=True)
    file_out = path.join(out_dir,
                         make_basename(files[0]))
    
    np.savetxt(file_out, np.column_stack((deltas, ks)),
               header = 'delta(mm)\texcess kurtosis')

ax.set_ylabel('excess kurtosis')
ax.set_xlabel('scale (mm)')
ax.legend(loc='best')