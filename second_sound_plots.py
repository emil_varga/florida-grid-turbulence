# -*- coding: utf-8 -*-
"""
Created on Wed Oct 12 12:26:05 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as path
from glob import glob
from parse import parse
import scipy.interpolate as interp

def sweep_to_timeseries(t, X, sweeptime, method):
    lw = t.min()
    tout = []
    xout = []
    while lw < t.max():
        ix = np.logical_and(t >= lw, t < lw + sweeptime)
        tt = t[ix]
        xx = X[ix]
        if method == 'max':
            I = xx.argmax()
            tout.append(tt[I])
            xout.append(xx[I])
        elif method == 'mean':
            tout.append(tt.mean())
            xout.append(xx.mean())
        else:
            raise ValueError("only 'max' and 'mean' methods are supported")
        
        lw += sweeptime
    return np.array(tout), np.array(xout)


plt.close('all')

def plot_ss_lines(data_dir):    
    peak_scan_fn = 'SS_*.dat'
    
    peak_fns = glob(path.join(data_dir, peak_scan_fn))
#    print(peak_fns, path.join(data_dir, peak_scan_fn))
    f, ax = plt.subplots(1,1)
    k=0
    for fn in peak_fns:
        k+=1
        print("{} : {} / {}".format(fn, k, len(peak_fns)))
        with open(fn) as file:
            header = file.readline()
        tok = parse("#frequency:{}\tphase:{}\tsweep{}", header)
    #    print(tok)
        d = np.loadtxt(fn)
        traw, yraw = sweep_to_timeseries(d[:,0], d[:,2], sweeptime=0.3, method='max')
        ax.clear()
        ax.plot(d[:,0], d[:,2], '-', color='blue')
        ax.plot(traw, yraw, '-o', color='red')
        f.suptitle(tok[2])
        f.savefig(fn.replace(".dat", ".png"))
    
    plt.close(f)
    
data_dirs = ['../data/T200/50mms/second-sound']
             
for d in data_dirs:
    plot_ss_lines(d)