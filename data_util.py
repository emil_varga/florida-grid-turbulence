# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 14:07:50 2016

@author: emil
"""
import re
import numpy as np
import os.path as path
import data_dirs
from glob import glob
from parse import parse
import tcal

def q_to_vns(q,T,A):
    """
        Calculates the counterflow velocity
        q in W, A in cm^2, result in cm/s
        returns vns, vn, vs
    """
    vns = q / (A*T*tcal.rs(T)*tcal.s(T))
    vn = vns*tcal.rs(T)/tcal.r(T)
    vs = vns*tcal.rn(T)/tcal.r(T)

    return vns, vn, vs

def m_quantity(s):
    """
        Conversion from base units to mili* (e.g., seconds to miliseconds).
        Input is a text string that possibly ends in 'm', result is an int.
        If the string already ends in m, the result is not multiplied.
    """
    if(s[-1] == 'm'):
        t = int(s[:-1])
    else:
        t = 1000*int(s)

    return t

#
# The next three functions get_* are meant for files in the processed/ directory.
# They don't work for raw data (images). Decode directories with dname_data for those
# and individual data files with fname_data
#

def get_time(fn):
    """
        Returns the decay time, in miliseconds, of a given processed file.
    """
    tok = re.split('[\_\.]', fn)
    time_s = next(t for t in tok if re.match(r"\d+ms$", t))
    t = int(time_s[:-2])
#    print("fn = {} : t = {}".format(fn, t))
    return t

def get_temp(fn):
    """
        Returns the temperature (in K) of a given processed file.
    """
    tok = re.split('[\_\.]', fn)
    T_s = next(t for t in tok if re.match(r"\d+K$", t))
    T = float(T_s[:-1])/100
#    print("fn = {} : t = {}".format(fn, t))
    return T

def get_vel(fn):
    """
        Returns the velocity (in mm/s) of a given processed file.
    """
    tok = re.split('[\_\.]', fn)
    v_s = next(t for t in tok if re.match(r"\d+mms$", t))
    v = float(v_s[:-3])
#    print("fn = {} : t = {}".format(fn, t))
    return v

#
# Parsing directory and file names of raw data.
#

def dname_data(dname):
    """
        Parses the data directory name with raw images for temperature (in K),
        velocity (in mm/s), decay time (in ms) and good and baseline image indices.

        Returns T, v, t, good_idx, base_idx
    """
    d = parse("{}/T{}/{}mms/images/{}", dname)
    T = float(d[1])/100
    v = float(d[2])
    t = m_quantity(d[3].split('_')[0][:-1])

    good_dir = path.join(dname , 'PNGs', 'GOOD')
    good_img = glob(path.join(good_dir, 'LHe*.png'))
    good_img_idx = [int(re.split('[/\.\-\_]', fn)[-3]) for fn in good_img]
    base_img_idx = np.loadtxt(path.join(good_dir, 'base'), dtype=int)
    return T, v, t, good_img_idx, base_img_idx

def fname_data(fname):
    """
        Parses the raw data file name for drift time (in ms) and file index.

        returns tdrift, idx
    """
    d = re.split('[//._]', fname)
    o = fname.find('drift')
    k=o
    while(not fname[k] == '_'):
        k = k-1
    tdrift = m_quantity(fname[k+1:o-1]) + 2 #the filenames are wrong
    idx = int(d[-2])

    return tdrift, idx

def get_files(which, T, vel='*', time='*', base_data_dir = data_dirs.get_base_data_dir('processed'),
              no_subsets=True, force_pattern=None, just_files=False):
    """
        Returns specified processed data set. T, vel (in mm/s), time (in ms) can be lists.

        returns 3 lists of file paths, decay times and temperatures
    """
    if T == '*':
        T_pat = [T]
    else:
        TT = np.atleast_1d(T)
        T_pat = ['{:.0f}'.format(T*100) for T in TT]

    if vel == '*':
        vel_pat = [vel]
    else:
        vv = np.atleast_1d(vel)
        vel_pat = ['{:d}'.format(vel) for vel in vv]

    if time == '*':
        time_pat = [time]
    else:
        tt = np.atleast_1d(time)
        time_pat = ['{:d}ms*'.format(time) for time in tt]

#    print("Patterns: ", T_pat, vel_pat, time_pat)
#    print("base dir: ", base_data_dir)
    files = []

    for Tp in T_pat:
        for vp in vel_pat:
            for tp in time_pat:
                dn = path.join(base_data_dir, which, "T{}".format(Tp))
                if force_pattern is None:
                    pat = path.join(dn, "*_{}K_{}mms_{}.dat".format(Tp, vp, tp))
                else:
                    pat = path.join(dn, force_pattern)
#                print("final pattern: ", pat)
                files += glob(pat)

    if just_files:
        return files

    if no_subsets:
        files = [f for f in files if f.find('subset') == -1]
    files.sort(key=lambda fn: 1000*get_time(fn) + get_temp(fn))
    times = [get_time(fn) for fn in files]
    Ts    = [get_temp(fn) for fn in files]

    return files, times, Ts

def vinc_delta(filename):
    """
        Reads the separation from the filename of velocity increments dump.

        Returns the separation in mm, 0 == velocities, not increments.
    """
    template = "DUMP_{}K_{}mms_{}ms_delta_{}.dat"
    basename = path.split(filename)[-1]
    try:
        T, v, t, delta = parse(template, basename)
    except:
        template = "DUMP_{}K_{}mms_{}ms_{}_delta_{}.dat"
        T, v, t, _, delta = parse(template, basename)

    delta = delta.replace('p', '.')

    return float(delta)

if __name__ == '__main__':
    files, ts, Ts = get_files('structure-function', [1.45, 1.65, 1.85], 300, [1000, 4000])
    for f, t, T in zip(files, Ts, Ts):
        print(f, t, T)