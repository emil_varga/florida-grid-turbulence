# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 15:55:43 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import spe2bin as spe
import os
from glob import glob
import scipy.ndimage as ndimg

def spe2bin_dir(dn):    
    files = glob(os.path.join(dn, 'LHe*.SPE'))
    files.sort()
    N = len(files)
    k = 0    
    
    for f in files:
        print("%s : %d / %d"%(dn, k, N))
        img = spe.read_datafile(f)
        data = np.column_stack((np.arange(1024), img))
        np.savetxt(f.replace("SPE", "dat"), data, fmt="%d")
        img = abs(np.log(img))
        img[np.isinf(img)] = 0
        img = ndimg.filters.gaussian_filter(img, 2)
        fig, ax = plt.subplots(1,1)
        ax.imshow(img, cmap='Greys_r')
        fig.savefig(f.replace(".SPE", "-0.png"))
        plt.close(fig)
        k = k+1

#dns = ['../data/T165/300mms/images/4000ms_1',
#       '../data/T165/300mms/images/8000ms',
#       '../data/T165/300mms/images/16000ms',
#       '../data/T165/50mms/images/1000ms_0',
#       '../data/T165/50mms/images/1000ms_1',
#       '../data/T165/50mms/images/2000ms',
#       '../data/T165/50mms/images/4000ms',
#       '../data/T165/50mms/images/8000ms']

#dns = ['../data/T185/300mms/images/2000ms',
#       '../data/T185/300mms/images/8000ms',
#       '../data/T145/300mms/images/1000ms',
#       '../data/T145/300mms/images/2000ms',
#       '../data/T145/300mms/images/4000ms',
#       '../data/T145/300mms/images/8000ms',
#       '../data/T145/50mms/images/1000ms',
#       '../data/T145/50mms/images/2000ms',
#       '../data/T145/50mms/images/4000ms',
#       '../data/T145/50mms/images/8000ms',
#       '../data/T185/50mms/images/1000ms',
#       '../data/T185/50mms/images/2000ms',
#       '../data/T185/50mms/images/4000ms',
#       '../data/T185/50mms/images/8000ms']
dns = ['../data/T185/0mms/images/0ms']

for dn in dns:
    spe2bin_dir(dn)
