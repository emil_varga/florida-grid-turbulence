# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 14:22:44 2016

@author: emil

Calculates the structure functions, spectra, flow and turbulent velocity profiles and
few basic statistics.
"""

import os
import random as rnd
import os.path as path
import sys
import re
from multiprocessing import Pool
from glob import glob
import numpy as np
from parse import parse
from functools import partial

import tcal

pixd = 11.2e-3 #mm, 11.2 um pixel distance

def q_to_vns(q,T,A): #q in W, A in cm^2, result in cm/s
    vns = q / (A*T*tcal.rs(T)*tcal.s(T))
    vn = vns*tcal.rs(T)/tcal.r(T)
    vs = vns*tcal.rn(T)/tcal.r(T)

    return vns, vn, vs

def m_quantity(s):
    if(s[-1] == 'm'):
        t = int(s[:-1])
    else:
        t = 1000*int(s)

    return t

def dname_data(dname):
    d = parse("{}/T{}/{}mms/images/{}", dname)
#    print(d)
    T = float(d[1])/100
    v = float(d[2])
    t = m_quantity(d[3].split('_')[0][:-1])

    good_dir = path.join(dname , 'PNGs', 'GOOD')
    good_img = glob(path.join(good_dir, 'LHe*.png'))
    good_img_idx = [int(re.split('[/\.\-\_]', fn)[-3]) for fn in good_img]
    base_img_idx = np.loadtxt(path.join(good_dir, 'base'), dtype=int)
    return T, v, t, good_img_idx, base_img_idx

def fname_data(fname):
    d = re.split('[//._]', fname)
    o = fname.find('drift')
    k=o
    while(not fname[k] == '_'):
        k = k-1
    tdrift = m_quantity(fname[k+1:o-1]) + 2 #the filenames are wrong
    idx = int(d[-2])

    return tdrift, idx

def process_dir(dname, badimg = None, use_old=True, allow_mv = True,
                elastic=False, subset=False, subset_index=0, sample_reduction=0.5):
    from scipy.stats import gaussian_kde
    import scipy.interpolate as interp
    from TracerLine import TracerLine
    import matplotlib.pyplot as plt

    img_index = lambda fn: int(re.split('[_.]', fn)[-2])

    imgfs = glob(path.join(dname, "LHe*.dat"))
    imgfs.sort(key=img_index)
    T_d, v_d, t_d, good_img_idx, base_img_idx = dname_data(dname)
    tdrift, idx = fname_data(imgfs[0])

    t_dec = t_d

#    if(t_d != dec_timef):
#        print("mismatch between file and dir data!")
#        print(dname); print(imgfs[0])
#        sys.exit(-1)

    is_baseline = False

    multi = re.search(r"\d+ms_\d+", dname)
    if multi is None:
        basename = "%dK_%dmms_%dms" % (T_d*100, v_d, t_dec)
    else:
        basename = "%dK_%dmms_%s" % (T_d*100, v_d, multi.group())

    print("MULTI = {}, BASENAME = {}".format(multi, basename))

    if subset:
        s = rnd.SystemRandom()
        np.random.seed(s.randint(0, 1000000))
        basename += '_subset{}'.format(subset_index)
        idx_choice = np.random.choice(good_img_idx, round(len(good_img_idx)*sample_reduction), replace=False)
    #try getting the mean profile
    fmvx = 'mvx_'+basename+".dat"
    mv_prof = None
    if allow_mv and path.isfile(fmvx):
        mvx = np.loadtxt(fmvx)
        mv_prof = interp.interp1d(mvx[:,0], mvx[:,1], kind='linear', bounds_error=False)
        print('Using mean velocity profile for '+dname)

    min_vels = []
    max_vels = []

    Nxbase = 512
    x_base = np.linspace(0, 1024, num=Nxbase)
    k_base = np.fft.fftfreq(len(x_base), np.diff(x_base).mean()*pixd)
    k_base.sort()
    mean_spectrum = np.zeros(k_base.shape, dtype=float)
    mean_spectrum_nore = np.zeros(k_base.shape, dtype=float)
    ssamples = np.zeros_like(k_base)
    sf_samples  = np.zeros_like(x_base) #structure function
    sf1         = np.zeros_like(x_base)
    sf2         = np.zeros_like(x_base)
    sf3         = np.zeros_like(x_base)
    sf4         = np.zeros_like(x_base)
    sf5         = np.zeros_like(x_base)
    sf6         = np.zeros_like(x_base)
    sf6         = np.zeros_like(x_base)
    sf7         = np.zeros_like(x_base)
    sf8         = np.zeros_like(x_base)
    sf9         = np.zeros_like(x_base)
    sf10        = np.zeros_like(x_base)
    samples = 0

    mvx = np.zeros_like(x_base)
    tix = np.zeros_like(x_base)
    vx_samples = np.zeros_like(x_base)

    velocity_samples = []
    NN = len(imgfs)

    if badimg is not None:
        bad_idx = np.loadtxt(badimg, dtype=int)
    else:
        bad_idx = []

    for fn in imgfs[:NN]:
        idx = img_index(fn)
        print("{} : {}/{}".format(dname, idx, NN))
        if not (idx in good_img_idx) or (idx in bad_idx):
            print('bad image, skipping...')
            continue;
        sys.stdout.flush()

        t_drf, fidx = fname_data(fn)
#        if t_dec != dec_timef or t_drf != tdrift or fidx != idx:
#            print('mismatch in file data!')
#            print(fn)
#            sys.exit(-1)

#        if(idx % 10 == 0):
        if idx in base_img_idx:
            is_baseline=True
        else:
            is_baseline=False
            #skip images not in the subset, but keep baselines regardless
            if subset and (idx not in idx_choice):
                print('image not in random selection, skipping...')
                continue;

        tl = TracerLine(fn, use_old=use_old, baseline=False, use_intensity=True)

        try:
            tl.find_ridge(kde_bw = 10, fancy_sigmas=False,
                          elastic=elastic, cleanup=True)
            tl.save_img(newpic=False)
            tl.save_line()
        except:
            print('Error in {}'.format(fn))
            raise

#        if (not is_baseline) and (not good_linep(tl)):
#            ignored_lines.append(idx)
#            continue

        line = tl.line_x

        if(is_baseline):
            baseline = line
        else:
            #calculate the velocities
            v_y = (baseline(x_base) - line(x_base))*pixd/tdrift*1000
            if len(v_y) != len(x_base):
                print("ERROR {}".format(fn))
                sys.exit(-1)

            #get rid of nans
            ix = np.logical_not(np.isnan(v_y))
            xsc = x_base[ix] #"clean" data
            vyc = v_y[ix]

            #cut the line to 600 pix around the centre
            mid = 0.5*(xsc.min() + xsc.max())
            ix_c = np.logical_and(xsc > (mid - 300),
                               xsc < (mid + 300))
            xsc = xsc[ix_c]
            vyc = vyc[ix_c]

            ix = np.arange(len(ix))[ix][ix_c]

            velocity_samples = np.append(velocity_samples,
                                      vyc[::4])

            #velocity profiles
            mvx[ix] += vyc
            tix[ix] += vyc**2
            vx_samples[ix] += 1
            min_vels.append(min(vyc))
            max_vels.append(max(vyc))

            #subtract the mean profile if we are using it
            if mv_prof is not None:
                vyc = vyc - mv_prof(xsc)
                ix  = np.logical_not(np.isnan(vyc))
                xsc = xsc[ix]
                vyc = vyc[ix]

            vyc = vyc - vyc.mean()
#            vyc = vyc / (len(vyc))

            #calculate structure function
            Dn = 1
            while Dn < len(xsc) - 1:
                kn = 0
                while kn < len(xsc) - Dn:
                    sf0 = abs(vyc[kn+Dn] - vyc[kn])
                    kn = kn + 1
                    sf1[Dn-1] = sf1[Dn-1] + sf0
                    sf2[Dn-1] = sf2[Dn-1] + sf0**2
                    sf3[Dn-1] = sf3[Dn-1] + sf0**3
                    sf4[Dn-1] = sf4[Dn-1] + sf0**4
                    sf5[Dn-1] = sf5[Dn-1] + sf0**5
                    sf6[Dn-1] = sf6[Dn-1] + sf0**6
                    sf7[Dn-1] = sf7[Dn-1] + sf0**7
                    sf8[Dn-1] = sf8[Dn-1] + sf0**8
                    sf9[Dn-1] = sf9[Dn-1] + sf0**9
                    sf10[Dn-1] = sf10[Dn-1] + sf0**10
                    sf_samples[Dn-1] = sf_samples[Dn-1] + 1
                Dn = Dn + 1

            #calculate the spectrum
            vel_k = np.fft.fft(vyc)/len(vyc)
            k    = np.fft.fftfreq(len(xsc), np.diff(xsc).mean()*pixd)
            ixx = np.argsort(k)
            vel_k = vel_k[ixx]
            k = k[ixx]

            #interpolate the spectrum onto a common base
            vvk = interp.interp1d(k, vel_k, bounds_error=False, copy=False)(k_base)
            #get rid of NaNs
            ix = np.logical_not(np.isnan(vvk))
            mean_spectrum[ix] = mean_spectrum[ix] + abs(vvk[ix])**2
            mean_spectrum_nore[ix] = mean_spectrum_nore[ix] + np.imag(vvk[ix])**2
            ssamples[ix] = ssamples[ix] + 1
            samples = samples + 1

    #save the structure function

    ix = sf_samples > 1
    sf1 = sf1[ix] / sf_samples[ix]
    sf2 = sf2[ix] / sf_samples[ix]
    sf3 = sf3[ix] / sf_samples[ix]
    sf4 = sf4[ix] / sf_samples[ix]
    sf5 = sf5[ix] / sf_samples[ix]
    sf6 = sf6[ix] / sf_samples[ix]
    sf7 = sf7[ix] / sf_samples[ix]
    sf8 = sf8[ix] / sf_samples[ix]
    sf9 = sf9[ix] / sf_samples[ix]
    sf10 = sf10[ix] / sf_samples[ix]
    samples = sf_samples[ix]
    n = len(sf1)
    xs = (np.arange(n) + 1)*np.diff(x_base).mean()*pixd
    fsf, axsf = plt.subplots(1,1)
    axsf.loglog(xs, sf2)
    axsf.set_xlabel(r'$\delta (cm)$')
    axsf.set_ylabel(r'$\overline{\langle v_y(x) - v_y(x+\delta)\rangle^2}$')
    fsf.savefig('../sf_'+basename+'.pdf')
    sfout = np.column_stack((xs, sf1, sf2, sf3, sf4, sf5, sf6, sf7, sf8, sf9, sf10, samples))
    np.savetxt('../im_sf_'+basename+'.dat', sfout)
    sfout = np.column_stack((xs, sf2, samples))
    np.savetxt("../sf_"+basename+".dat", sfout)

    ix = ssamples > 1

    mean_spectrum = mean_spectrum[ix] / ssamples[ix]
    mean_spectrum_nore = mean_spectrum_nore[ix] / ssamples[ix]
    samples = ssamples[ix]
    k_base = k_base[ix]

    ix = k_base > 0
    mean_spectrum = mean_spectrum[ix]
    mean_spectrum_nore = mean_spectrum_nore[ix]
    samples = samples[ix]
    k_base = k_base[ix]

    f, ax = plt.subplots(1,1)
    ax.loglog(2*np.pi*k_base, mean_spectrum, lw=3, label="<.$^2$>")
    ax.loglog(2*np.pi*k_base, 1e-1/k_base**2, lw=3, label="k$^{-2}$")
    ax.loglog(2*np.pi*k_base, mean_spectrum_nore, lw=3, label="imag part only")
    ax.legend(loc='best')
    ax.set_ylim([0.9*mean_spectrum.min(), 1.1*mean_spectrum.max()])
    plt.savefig("../spectra_"+basename+".pdf")

    np.savetxt('../mean_Ek_'+basename+'.dat', np.column_stack((2*np.pi*k_base, mean_spectrum, samples)))
    np.savetxt('../mean_Ek_nore_'+basename+'.dat', np.column_stack((2*np.pi*k_base, mean_spectrum_nore, samples)))

    max_vel = np.mean(max_vels)
    min_vel = np.mean(min_vels)
    std_vel = np.std(velocity_samples)
    med_vel = np.median(velocity_samples)
    mean_vel = np.mean(velocity_samples)

    out = [mean_vel, std_vel, med_vel, max_vel, min_vel]
    np.savetxt("../velstats_"+basename+".dat", out, header='mean\tstd\tmedian\tmax\tmin')

    vpdf_kde = gaussian_kde(velocity_samples);
    vbase = np.linspace(velocity_samples.min(), velocity_samples.max(), 256)
    vpdf = vpdf_kde(vbase)

    vbase = vbase / std_vel

    out = np.column_stack((vbase, vpdf))
    np.savetxt("../velocity_pdf_"+basename+".pdf", out)

    f, ax = plt.subplots(1,1)
    ax.semilogy(vbase, vpdf, label = 'velocity PDF')
    ax.set_xlabel("$v$ / std($v$)")
    ax.set_ylabel("PDF")
    ax.legend(loc='best')
    plt.savefig("../vpdf"+basename+".eps")

    ix = vx_samples > 1
    mvx = mvx[ix] / vx_samples[ix]
    tix = tix[ix] / vx_samples[ix]
    tix = np.sqrt(tix - mvx**2)
    samples = vx_samples[ix]

    np.savetxt('../mvx_'+basename+'.dat', np.column_stack((x_base[ix], mvx, samples)))
    np.savetxt('../tix_'+basename+'.dat', np.column_stack((x_base[ix], tix, samples)))

if __name__ == "__main__":
    data = { 2.15 : {300 : [1000, 4000]},
             2.00 : {300 : [500, 1000, 4000],
                     50  : [500, 2000]},
             1.85 : {300 : [1000, 2000, 4000, 8000],
                     50  : [1000, 2000, 4000, 8000],
                     0   : [0]},
             1.65 : {300 : [1000, 2000, 4000, 8000, 16000],
                     50  : [1000, 2000, 4000, 8000]},
             1.45 : {300 : [1000, 2000, 4000, 8000],
                     50  : [1000, 2000, 4000, 8000]}}
#    data = {1.65 : {300 : [1000, 2000, 4000, 8000, 16000]}}
#    data = {1.65 : {300 : [0]}}
    dirs = []
    for T in data:
        for v in data[T]:
            for t in data[T][v]:
                dirs += glob('../data/T{:.0f}/{}mms/images/{}ms*'.format(100*T, v, t))
    def sort_key(dn):
        T, v, t, _, _ = dname_data(dn)
        return t + 1e4*v + 1e8*T
    dirs.sort(key=sort_key)

#    import cProfile
#    cProfile.run("process_dir('data/T145/150mms/500ms', use_old=False, elastic=True)")
#    process_dir('data/T145/150mms/500ms', use_old=False)
#    from functools import partial
#    with Pool(4) as p:
#        p.map(partial(process_dir, use_old=False), dirs)
#    with Pool(4) as p: #the second time for mean subtraction
#        p.map(partial(process_dir, subset=False, subset_index=0), dirs)

#    with Pool(2) as p: #the second time for mean subtraction
#        p.map(partial(process_dir, subset=True, subset_index=1), dirs)
#
#    with Pool(2) as p: #the second time for mean subtraction
#        p.map(partial(process_dir, subset=True, subset_index=2), dirs)
#
#    with Pool(2) as p: #the second time for mean subtraction
#        p.map(partial(process_dir, subset=True, subset_index=3, sample_reduction=0.25), dirs)
#
#    with Pool(2) as p: #the second time for mean subtraction
#        p.map(partial(process_dir, subset=True, subset_index=4, sample_reduction=0.25), dirs)
#
#    with Pool(2) as p: #the second time for mean subtraction
#        p.map(partial(process_dir, subset=True, subset_index=5, sample_reduction=0.25), dirs)

