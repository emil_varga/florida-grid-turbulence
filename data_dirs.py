#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 14:20:40 2017

@author: emil
"""

import platform
import os.path as path
import os

def get_base_data_dir(which='data'):
    """
        Returns the absolute path to the base directory where experimental data is stored.
    """
    node = platform.uname().node

    if node == 'emil-simulations':
        #the simulations computer, the working copy is on RAID
        return path.join('/media/Raid/Grid-turbulence/ss+lif/', which)
    elif node == 'emil-laptop':
        #notebook
        return path.join('/home/emil/Documents/fyzika/data/Grid-turbulence/ss+lif', which)
    else:
        raise RuntimeError('Specify data directory for this computer in data_dirs.py')

def get_cache_dir(run):
    """
    Returns the absolute path to the directory where to store cached results.
    """

    cache_dir = path.join(get_base_data_dir(), run, '.cache')

    if not path.exists(cache_dir):
        os.makedirs(cache_dir)

    return cache_dir