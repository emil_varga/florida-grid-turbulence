# -*- coding: utf-8 -*-
"""
Created on Sun Nov 13 16:35:06 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as interp
import data_dirs
import os.path as path
cm = plt.get_cmap('hsv')
plt.close('all')

from data_util import get_files, get_time, get_temp

T = 1.65
vel = 300
kappa = 0.997e-3 * 100 # mm^2/s
nu = 0.35*kappa
M = 1.27
L = 9.5

files, times, temps = get_files('spectrum', T=T, vel=vel)
files2, times2, temps2 = get_files('turbulence-intensity-profile', T=T, vel=vel)
files3, times3, temps3 = get_files('mean-velocity-profile', T=T, vel=vel)
print(files, files2)

dn = data_dirs.get_base_data_dir(which='processed')
L_data = np.loadtxt(path.join(dn, 'second-sound', 'L_165K_300mms.dat'))

dis_L = interp.interp1d(L_data[:,0], (kappa*L_data[:,1]/100)**2*nu)

f, ax = plt.subplots(1,1)
ax.loglog(L_data[:,0], dis_L(L_data[:,0]))

k=0
N = len(files)
fig, ax = plt.subplots(1,1)
f2, ax2 = plt.subplots(1,1)
f3, ax3 = plt.subplots(1,1)

ts = []
diss = []
taylors = []
R_taylors = []
taylors_L = []
R_taylors_L = []
us = []
Us = []
for (fn, fn2, fn3) in zip(files, files2, files2):
    d = np.loadtxt(fn)
    k = d[:,0]
    E = 0.5*d[:,1]
    Es = interp.UnivariateSpline(np.log(k), np.log(E), s=0.05*len(k))
    ax2.semilogy(k, E, 'o')
    ax2.semilogy(k, np.exp(Es(np.log(k))))
    t = get_time(fn)/1000

    EE = np.exp(Es(np.log(k)))

    E3d = 0.5*k**3*np.gradient(np.gradient(EE)/np.gradient(k)/k)/np.gradient(k)
    ix = np.logical_and(k > 1, k < 100)
    ax3.loglog(k[ix], (E3d)[ix])

    d2 = np.loadtxt(fn2)
    d3 = np.loadtxt(fn3)
    x = d2[:,0]
    ixx = np.logical_and(x > 500, x < 700)
    u = d2[ixx,1]
    u2 = (u**2).mean()
    us.append(np.sqrt(u2))
    Us.append(d3[ixx,1].mean())
#    print(u.mean())
#    ax2.plot(x, u)
    ax.loglog(k[ix], (E3d*k**2)[ix])

    dis = np.trapz(x=k[ix], y=2*nu*E3d[ix]*k[ix]**2)
    taylor = np.sqrt(15*nu*u2/dis)
    R_taylor = np.sqrt(u2)*taylor/nu

    taylor_L = np.sqrt(15*nu*u2/dis_L(t))
    R_taylor_L = np.sqrt(u2)*taylor_L/nu

    ts.append(get_time(fn)/1000)
    diss.append(dis)
    taylors.append(taylor)
    R_taylors.append(R_taylor)
    taylors_L.append(taylor_L)
    R_taylors_L.append(R_taylor_L)

f, ax = plt.subplots(1,1)
ax.loglog(ts, diss, '-o')
ax.loglog(ts, dis_L(ts), '-o')
f.suptitle("dissipation")

us = np.array(us)
Us = np.array(Us)
f, ax = plt.subplots(1,1)
ax.loglog(ts, R_taylors, '-o', label='Re$_\lambda$')
ax.loglog(ts, R_taylors_L, '-s', label='Re$_\lambda$-L')
ax.loglog(ts, M*us/nu, '-o', label='Re$_M$')
ax.loglog(ts, L*Us/nu, '-o', label='Re$_L$')
ax.axhline(M*300/nu)
ax.legend(loc='best')
f.suptitle("Reynolds number")

f, ax = plt.subplots(1,1)
ax.loglog(ts, taylors, '-o')
f.suptitle("Taylor microscale")