# -*- coding: utf-8 -*-
"""
Created on Thu Mar 10 09:58:59 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt

import PRB_style

from data_util import get_files, get_time, get_temp, get_vel

import matplotlib

pixd = 11.2e-3 #mm, 11.2 um pixel distance

plt.close('all')

Ts   = [1.45, 1.65, 1.85, 2.0, 2.15]
vel = 50
time = '*'#[1000, 4000, 8000]


fsl, asl = plt.subplots(1,1) #structure function slopes

for T in Ts:
    fsf, axsf = plt.subplots(1,1) #structure functions

    files, times, Ts = get_files('structure-function', T=T, vel=vel,
                                 time=time,
                                 no_subsets=True)


    slopes = []
    slopes_err = []
    lines = []
    labels = []
    times = []

    colors = {145: '-o',
              165: '-s',
              185: '-^',
              200: '-d',
              215: '-v'}

    for fn in files:
        print(fn)
        t = get_time(fn)
        TT = get_temp(fn)

        d = np.loadtxt(fn)

        x = d[:,0]
        S = d[:,1]

        ix = np.logical_and(x>0.1, x<1)
        p, V = np.polyfit(np.log(x[ix]), np.log(S[ix]), deg=1, cov=True)
        slopes.append(p[0])
        slopes_err.append(np.sqrt(V[0,0]))
        times.append(t)

        axsf.loglog(x, S, colors[100*TT], label = '{:.2f} K'.format(TT, t, p[0]),
                    ms = 6, mec = 'black', mew = 0.5, lw=1)
        axsf.loglog(x, np.exp(p[1])*pow(x, p[0]), color='k')
        S1 = S/x
        S23 = S/x**(2/3)
        labels.append('{:.2f} K, {} ms'.format(TT, t))

    axsf.set_xlabel('$x$ (mm)')
    axsf.set_ylabel("$S_2$ (mm$^2$/s$^2$)")
    fsf.savefig('SF_{:.0f}_{}mms.pdf'.format(100*T, vel))


    times = np.array(times)
    slopes = np.array(slopes)
    slopes_err = np.array(slopes_err)
    asl.errorbar(times/1000, slopes, yerr=slopes_err, fmt=colors[100*T][1:],
                 ms = 6, mec = 'black', mew = 0.5, lw=1)

asl.set_xlabel('time (s)')
asl.set_ylabel('$\zeta_2$')
asl.axhline(2.0/3.0, ls='--')
fsl.savefig('S2_exponents_{}mms.pdf'.format(vel))