#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 12:53:59 2018

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt

import PRB_style

import data_util as util

plt.close('all')

pixd = 11.2e-3 #mm, 11.2 um pixel distance

T = 1.45
vel = 50

files, times, _ = util.get_files('velocity-profile', T, vel)
files2, times, _ = util.get_files('turbulence-intensity-profile', T, vel)

f1, ax = plt.subplots(1,1)
f2, ax2 = plt.subplots(1,1)

fmts = ['-', '--', ':', '-.', '-']

for (fn, fn2, stl) in zip(files, files2, fmts):
    t = util.get_time(fn)
    d = np.loadtxt(fn)
    d2 = np.loadtxt(fn2)

    x = d[:,0]*pixd
    mvx = d[:,1]
    tix = d2[:,1]
    print(tix.mean())

    ax.plot(x, mvx, stl, label = '{} s'.format(t/1000))
    ax2.plot(x, tix, stl)

ax.set_xlabel(r"$x$ (mm)"); ax2.set_xlabel(r"$x$ (mm)");
ax.set_ylabel(r"$v(x)$")
ax2.set_ylabel(r"$\sigma_v(x)$")
ax.legend(loc='best')
ax2.legend(loc='best')

f1.tight_layout()
f2.tight_layout()
f1.savefig('mvx_{}mms_{:.0f}K.pdf'.format(vel, 100*T))
f2.savefig('tix_{}mms_{:.0f}K.pdf'.format(vel, 100*T))

plt.show()
