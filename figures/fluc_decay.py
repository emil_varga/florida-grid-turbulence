# -*- coding: utf-8 -*-
"""
Created on Wed Sep 14 12:35:53 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import os

import PRB_style

from data_util import get_files

def plot_udec(T, v, ax, which='turbulence-intensity-profile', plt_style='-o'):
    files, dec_times, _ = get_files(which, T, vel=v)
    print(files)
    us = []
    ts = []
    for (f, t) in zip(files, dec_times):
        if os.path.isfile(f):
            data = np.loadtxt(f)
#            print(f, data.shape)
            us.append(data[:,1].mean())
            ts.append(t)
    us = np.array(us)
    ts = np.array(ts)
    print(us)
    print(ts)
    if which == 'turbulence-intensity-profile':
        ax.loglog(ts/1000, us**2, plt_style, label = '{:.2f} K'.format(T),
                  ms = 6, mec = 'black', mew = 0.5, lw=1)
    else:
        ax.plot(ts/1000, us, plt_style, label = '{:.2f} K'.format(T),
                ms = 6, mec = 'black', mew = 0.5, lw=1)

plt.close('all')

f, ax = plt.subplots(1,1)
plot_udec(1.45, 300, ax, plt_style = '-o')
plot_udec(1.65, 300, ax, plt_style = '-s')
plot_udec(1.85, 300, ax, plt_style = '-^')
plot_udec(2.00, 300, ax, plt_style = '-v')
plot_udec(2.15, 300, ax, plt_style = '-d')

ax.set_xlabel('$t$ (s)')
ax.set_ylabel('$\\langle u^2\\rangle$ (mm$^2$/s$^2$)')
ax.legend(loc='best')
#ax.grid('on', 'both')
t = np.linspace(1, 9)
ax.loglog(t, 100/t**2, color='k', lw=5)
f.tight_layout()
f.savefig('u2_decay.pdf')


f, ax = plt.subplots(1,1)
plot_udec(1.45, 300, ax, which='velocity-profile')
plot_udec(1.65, 300, ax, which='velocity-profile')
plot_udec(1.85, 300, ax, which='velocity-profile')
plot_udec(2.00, 300, ax, which='velocity-profile')
plot_udec(2.15, 300, ax, which='velocity-profile')

ax.set_xlabel('$t$ (s)')
ax.set_ylabel('$\\langle U\\rangle$ (mm/s)')
ax.legend(loc='best')
#ax.grid('on', 'both')
f.tight_layout()
f.savefig('mv_decay.pdf')