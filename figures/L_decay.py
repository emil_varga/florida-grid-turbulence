# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 13:50:54 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import scipy.optimize as optim

import PRB_style

import data_dirs
import os.path as path
plt.close('all')

import tcal
T = 1.85
kappa = 0.997e-3 #cm^2/s
D = 0.95 #cm, channel width
C = 1.5 # Kolmogorov constant
B = tcal.B(T)
rhos = tcal.rs(T)
rho  = tcal.r(T)


data_dir = data_dirs.get_base_data_dir(which='processed')
fn = path.join(data_dir,
               'second-sound', 'L_{:.0f}K_300mms.dat'.format(100*T))
print(fn)
d = np.loadtxt(fn)

t0 = 0
ix = np.logical_and(d[:,0] > t0, d[:,1] > 0)
t = d[ix,0] - t0
L = d[ix,1]


fig, ax = plt.subplots(1,1)
#ax.grid('on', 'both')

ax.loglog(t, L, 'o', mec='black', markeredgewidth=0.75, markersize=5, label='data')
#ax.loglog(t, 1e5*t**(-1.5), label = '$t^{-3/2}$')

def f(t, a, b, t0):
    return np.log(a*(t-t0)**(-1.5) + b)

ix = np.logical_and(t > 1, t<20)
popt, pcov = optim.curve_fit(f, t[ix], np.log(L[ix]), p0=(1e3,0,0))
#print(popt, pcov)

ff, axx = plt.subplots(1,1)
axx.loglog(t - popt[2], L - popt[1], label="$L(t - t_0) - L_0$, from the non-linear fit")
axx.loglog(t, popt[0]*(t)**(-1.5),
           label="$t^{-3/2}$ with prefactor from non-linear fit")
axx.loglog(t - 2.12, L, label="$L(t-t_0)$ with \"manual\" $t_0$ = 2.12 s, no $L_0$")
axx.legend(loc='best')
axx.set_xlim((0.1, 40))
axx.set_xlabel("$t$ (s)")
axx.set_ylabel("$L$ (cm$^{-2}$)")
ff.savefig("shifted_L.pdf")
A32 = popt[0]
A32e = np.sqrt(pcov[0,0])
A320 = (D*(3*C)**(1.5)/(2*np.pi*kappa*np.sqrt(kappa*0.33)))
print(A32, A320)

print(D, C, kappa)
nu = D*(3*C)**(1.5)/(2*np.pi*kappa*A32)
nu = nu**2
nu_e = 2*(D*(3*C)**(1.5)/(2*np.pi*kappa*A32))**2*A32e/A32
t0 = popt[2]
print("Viscosity and t0: {}\t{}\t{}".format(nu/kappa, nu_e/kappa, t0))

L_fit32 = np.exp(f(t, *popt))
#L_fit32 = f(t, *popt)

def f2(t, a, b, t0):
    return np.log(a*(t-t0)**(-5.0/6.0) + b)

ix = np.logical_and(t>0.2, t<2)
popt2, pcov = optim.curve_fit(f2, t[ix], np.log(L[ix]), p0=(1e3,1e3,0))
#print(popt, pcov)

L_fit56 = np.exp(f2(t, *popt2))

#L_fit = X[:,0]*x[0] + X[:,1]*x[1] + X[:,2]*x[2]
#L_fit = np.exp(X[:,0]*x[0] + X[:,1]*x[1])
ix = t < 2
ax.loglog(t, L_fit32, color='r', lw=2, label="fit $t^{-3/2}$")
ax.loglog(t[ix], L_fit56[ix], '--', color='cyan', lw=2, label="fit $t^{-5/6}$")
ax.set_xlim([4e-2, 40])
ax.set_ylim([2e2, 2e6])
ax.set_xlabel("$t$ (s)")
ax.set_ylabel("$L$ (cm$^{-2}$)")
ax.legend(loc='best')
fig.tight_layout()
fig.savefig('L_decay.pdf')

#fig, ax = plt.subplots(1,1)
#fig.suptitle('inter-vortex spacing (mm)')
#ax.loglog(t, 2*np.pi*0.1*np.sqrt(L))
#ax.set_xlabel('t (s)')
#ax.set_ylabel('$k_l$ (2$\pi$/mm)')

f, ax = plt.subplots(1,1)
ax.plot(t, L**(-2.0/3), label="no $L_0$ subtraction")
ix = np.logical_and(t > 1, t<20)
p = np.polyfit(t[ix], L[ix]**(-2.0/3), deg=1)
A32 = p[0]**(-3./2)
nu = D*(3*C)**(1.5)/(2*np.pi*kappa*A32)
nu = nu**2
nu_e = 2*(D*(3*C)**(1.5)/(2*np.pi*kappa*A32))**2*A32e/A32
print("t0 = {}, nu = {}".format(-p[1]/p[0], nu/kappa))
ax.plot(t, np.polyval(p, t), label="fit in (5, 20) s interval", color='g')
ax.plot(t, (L - popt[1])**(-2./3), label="subtracted $L_0$ = {:.0f}".format(popt[1]),
        color='red')
p = np.polyfit(t[ix], (L[ix]-popt[1])**(-2.0/3), deg=1)
print("check t0 = {}".format(-p[1]/p[0]))
ax.plot(t, np.polyval(p, t), label="fit in (5, 20) s interval", color='g')
ax.legend(loc='best')
ax.set_xlim((0, 30))
ax.set_ylim((0, 0.025))
ax.set_xlabel("$t$ (s)")
ax.set_ylabel("$L^{-2/3}$")
f.savefig("inverted_L.pdf")
