#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 12:13:35 2018

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt

import PRB_style

from data_util import get_files, get_temp


import itertools

plt.close('all')

vels = [50, 300]
times = [500, 1000, 2000, 4000, 8000, 16000]
Ts = [1.45, 1.65, 1.85, 2.0, 2.15]

for vel in vels:
    for time in times:
        for T in Ts:
            files, ts, temps = get_files('intermittency', T=T, vel=vel, time=time)
            print(files)
            if len(files) == 0:
                continue

            fig_ess, ax_ess = plt.subplots(1,1)
            ax_ess.set_xlabel('$S_3$ (a.u.)')
            ax_ess.set_ylabel('$S_n$ (a.u.)')

            xmin = 0.2
            xmax = 4

            k=0

            zetas = {}
            offset=1
            ranges = {}
            for fn in files:
                d = np.loadtxt(fn)

                r = d[:,0]
                S3 = abs(d[:,3])

                for n in range(7):
                    Sn = abs(d[:, n+1])

                    ax_ess.loglog(S3, offset*Sn, '-o', markersize = 6, markeredgecolor="none",
                               label = '{}'.format(n+1))
                    offset *= 1
                k += 1

            ax_ess.legend(loc='best')
            fig_ess.savefig("ess_{:.0f}K_{}mms_{}ms.pdf".format(100*T, vel, time))
