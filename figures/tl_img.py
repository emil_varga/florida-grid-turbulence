# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 15:55:43 2016

@author: emil
"""

import numpy as np
import scipy.ndimage as ndimg
import matplotlib.pyplot as plt

import os.path as path
from glob import glob

import PRB_style

import spe2bin as spe
import data_dirs as dirs
from TracerLine import TracerLine

pixd = 11.2e-3 #mm, 11.2 um pixel distance

plt.close('all')

base_dir = dirs.get_base_data_dir()
data_dir = path.join(base_dir, 'T185', '300mms', 'images', '2000ms')

files = glob(path.join(data_dir, 'LHe*.SPE'))
files.sort()
N = len(files)

f = files[0]

######
###### Uncomment this to get tl_base and tl_flow into memory
###### once thay exist, it can be commented out again to make the script finish faster
#tl_base = TracerLine(files[0].replace('.SPE', '.dat'), use_old=False, baseline=True,
#                     use_intensity=True)
#tl_base.find_ridge(kde_bw = 10, fancy_sigmas=False, elastic=False, cleanup=True)
#
#tl_flow = TracerLine(files[3].replace('.SPE', '.dat'), use_old=False, baseline=False,
#                     use_intensity=True)
#tl_flow.find_ridge(kde_bw = 10, fancy_sigmas=False, elastic=False, cleanup=True)


base_img = spe.read_datafile(files[0])
base_img= abs(np.log(base_img))
base_img[np.isinf(base_img)] = 0
base_img = ndimg.filters.gaussian_filter(base_img, 2)

flow_img = spe.read_datafile(files[3])
flow_img= abs(np.log(flow_img))
flow_img[np.isinf(flow_img)] = 0
flow_img = ndimg.filters.gaussian_filter(flow_img, 2)

fig, (ax1, ax2) = plt.subplots(2,1, sharex=True, sharey=True)
ticks = np.array([1, 256, 512, 768, 1024])*pixd
ax2.set_xticks(ticks)

extent = [0, 1024*pixd, 0, 1024*pixd]
origin = 'lower'
ax1.imshow(flow_img, cmap='Greys_r', extent=extent, origin=origin)

ax2.imshow(base_img, cmap='Greys_r', extent=extent, origin=origin)
ax2.set_ylim(6, 8)

ax1.plot(tl_flow.line_points[:,0]*pixd, tl_flow.line_points[:,1]*pixd, lw=1, color='r')
ax2.plot(tl_base.line_points[:,0]*pixd, tl_base.line_points[:,1]*pixd, lw=1, color='r')

ax2.set_xlabel('$x$ (mm)')
ax2.set_ylabel('$y$ (mm)')
ax1.set_ylabel('$y$ (mm)')

fig.tight_layout()
fig.savefig('tracer-lines.pdf')