#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  9 09:37:50 2017

@author: emil
"""

import matplotlib as mp

mp.rcParams['figure.figsize'] = (3.4, 2.55)
mp.rcParams['figure.autolayout'] = True
mp.rcParams['xtick.top'] = True
mp.rcParams['xtick.bottom'] = True
mp.rcParams['xtick.direction'] = 'in'
mp.rcParams['ytick.left'] = True
mp.rcParams['ytick.right'] = True
mp.rcParams['ytick.direction'] = 'in'

mp.rcParams['ytick.labelsize'] = 7
mp.rcParams['xtick.labelsize'] = 7
mp.rcParams['font.size'] = 8

prb_fig_width = 3.4

import sys
sys.path.append('..')
