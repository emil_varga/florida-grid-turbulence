# -*- coding: utf-8 -*-
"""
Created on Thu Mar 10 09:58:59 2016

@author: emil
"""

from numpy import *
import matplotlib.pyplot as plt
from glob import glob
from scipy.stats import linregress
from scipy.interpolate import interp1d
import os.path as path
import re

import matplotlib
matplotlib.rcParams.update({'font.size': 12})
matplotlib.rc('text', usetex=True)

pixd = 11.2e-3 #mm, 11.2 um pixel distance

plt.close('all')

#Jian's spectra
fnj_es = {0    : 'Energy spectrum_SS.txt',
          1000 : 'Energy spectrum_1s.txt',
          100 : 'Energy spectrum_100ms.txt',
          4000 : 'Energy spectrum_4s.txt'}

#Jian's structure functions
fnj_sf = {0    : 'Structure function_SS.txt',
          1000 : 'Structure function_1s.txt',
          100 : 'Structure function_100ms.txt',
          4000 : 'Structure function_4s.txt'}

fs, axs = plt.subplots(1,1, figsize=(10,6)) #spectra
fsc, (axsc2, axsc53) = plt.subplots(1,2, sharex=True) #compensated

fsf, axsf = plt.subplots(1,1, figsize=(10,6)) #structure functions
fsfc, (axsfc1, axsfc23) = plt.subplots(1,2, sharex=True) #compensated

def get_time(fn):
    t = int(re.split('[\_\.]', fn)[-2][:-2])
    print("fn = {} : t = {}".format(fn, t))
    return t
#    time_string = fn.split('_')[-1]
#    return int(time_string.split('ms')[0])

#structure functions
#files = glob('sf_*.dat')
#files.sort(key=get_time)
#files = list(map(lambda n: "sf_"+str(n)+".dat",
#                 [500, 1000, 2000]))
files = ["../sf_165K_300mms_%dms.dat" % n for n in [1000, 2000, 4000]]

lines = []
labels = []

NUM_COLORS = len(files)
cm = plt.get_cmap('hsv')

#from cycler import cycler
axs.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
axsc2.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
axsc53.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

slopes = []
slopes_err = []
slopes_sf = []
slopes_sf_err = []
times = []

n = 0
for fn in files:
    t = get_time(fn)
    d = loadtxt(fn)
    
    x = d[:,0]
    S = d[:,1]
    try:
        fnj = fnj_sf[t]
    except:
        fnj = ""
    if path.isfile(fnj):
        dj = loadtxt(fnj)
        nj = 1/100#interp1d(x, S)(0.25) / interp1d(dj[:,0], dj[:,1])(2.5)
        axsf.loglog(dj[:,0], dj[:,1], '-o', color='0.7', lw=3)    
    
    ix = logical_and(x>0.4, x<2.25)
    p, V = polyfit(log(x[ix]), log(S[ix]), deg=1, cov=True)
    slopes.append(p[0])
    slopes_err.append(sqrt(V[0,0]))
    times.append(t)
    
    axsf.loglog(x, exp(p[1])*pow(x, p[0]), color='0.5')
    axsf.loglog(x, S, '-o', label = '{} ms (exp: {:.2f})'.format(t, p[0]))
    S1 = S/x
    S23 = S/x**(2/3)
    lines.append(axsfc1.loglog(x, S1, label = '{} ms'.format(t))[0])
    axsfc23.loglog(x, S23, label = '{} ms'.format(t))
    labels.append('{} ms'.format(t))
    n = n+1
   
axs.grid('on')
axs.set_xlabel('$x$ (mm)')
axsfc1.set_xlabel('$x$ (mm)')
axsfc23.set_xlabel('$x$ (mm)')

axs.set_ylabel("$S(cm)$")

axsf.legend(loc='best')

axsf.set_title('Structure function')
axsfc1.set_title('$S/x$')
axsfc23.set_title('$S/x^{2/3}$')
axsfc1.grid('on'); axsfc23.grid('on')
lgd = fsfc.legend(lines, labels, loc='center right', ncol=1, bbox_to_anchor=[1.0, 0.25])
fsfc.subplots_adjust(right=0.85)
#axsf.set_xlim((1e-1, 1e1))
#axsf.set_ylim((1e-1, 1e3))
axsfc1.set_xlim((1e-1, 1e1))
fsf.savefig('SF.eps')
fsfc.savefig('SF_compensated.eps', bbox_extra_artists=(lgd,))#, bbox_inches='tight')

times = array(times)
slopes = array(slopes)
slopes_err = array(slopes_err)
fsl, asl = plt.subplots(1,1)
asl.errorbar(times, slopes, yerr=slopes_err)
asl.set_xlim([-100, 4100])
asl.set_xlabel('time (ms)')
asl.set_ylabel('SF exponent')
asl.set_title('SF exponent')
fsl.savefig('SF_exponents.eps')

#spectra
files = ["../mean_Ek_165K_300mms_%dms.dat" % n for n in 
            [1000, 2000, 4000]]


lines = []
labels = []

NUM_COLORS = len(files)
cm = plt.get_cmap('hsv')
axs.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
axsc2.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
axsc53.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

slopes = []
slopes_err = []
slopes_sf = []
slopes_sf_err = []
times = []

n = 0
for fn in files:
    t = get_time(fn)
    d = loadtxt(fn)
    print(max(d[:,0]))
    ix = logical_and(d[:,0] > 0, d[:,0] < 30)
    
    basename = fn.split('mean_Ek')[1]
#    vstats_fn = 'velstats'+basename
#    vstats = loadtxt(vstats_fn)
#    sig = vstats[1]
    
    k  = d[ix,0]
    Ek = d[ix,1]
    
#    ix = k < 70
#    k = k[ix]
#    Ek = Ek[ix]
#   Jian's data
    try:
        fnj = fnj_es[t]
    except:
        fnj = ""
    if path.isfile(fnj):
        dj = loadtxt(fnj)
        nj = interp1d(k, Ek)(5)/interp1d(dj[:,0], dj[:,1])(5)
        axs.loglog(dj[:,0], dj[:,1]*nj, '-o', color='0.7', lw=3)
    
    ix = logical_and(k>1, k<8)
    p, V = polyfit(log(k[ix]), log(Ek[ix]), deg=1, cov=True)
    slopes.append(p[0])
    slopes_err.append(sqrt(V[0,0]))
    times.append(t)
    
    axs.loglog(k, exp(p[1])*pow(k, p[0]), color='0.5')
    axs.loglog(k, Ek, '-o', label = '{} ms (exp: {:.2f})'.format(t, p[0]))
    
    lines.append(axsc2.loglog(k, Ek*(k**2), label = '{} ms'.format(t))[0])
    axsc53.loglog(k, Ek*(k**(5/3)), label = '{} ms'.format(t))
    labels.append('{} ms'.format(t))
#    n = n + 1

#d = loadtxt('00mean_Eklines.dat')
#axs.loglog(d[:,0]*2*pi/10, d[:,1], color='blue', lw=3)
axs.grid('on')
axs.set_xlim([0.75, 30])
axs.set_xlabel('$k$ (2$\pi$/mm)')
axsc53.set_xlabel('$k$ (2$\pi$/mm)')

axs.set_ylabel("$E(k)$")
axsc2.set_xlim([0.75, 30])

axs.legend(loc='best')

axs.set_title('Energy spectrum')
axsc2.set_title('$Ek^2$')
axsc53.set_title('$Ek^{5/3}$')
axsc2.grid('on'); axsc53.grid('on')
lgd = fsc.legend(lines, labels, loc='center right', ncol=1, bbox_to_anchor=[1.0, 0.75])
fsc.subplots_adjust(right=0.85)
fs.savefig('spectra.eps')
fsc.savefig('spectra_compensated.eps', bbox_extra_artists=(lgd,))#, bbox_inches='tight')

times = array(times)
slopes = array(slopes)
slopes_err = array(slopes_err)
fsl, asl = plt.subplots(1,1)
asl.errorbar(times, slopes, yerr=slopes_err)
asl.set_xlim([-100, 4100])
asl.set_xlabel('time (ms)')
asl.set_ylabel('energy spectrum exponent')
asl.set_title('Energy spectrum exponent')
fsl.savefig('exponents.eps')

#PDFs
files = glob('velocity_pdf*.dat')

files.sort(key=get_time)

fpdf, axpdf = plt.subplots(1,1)
axpdf.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

for fn in files:
    t = get_time(fn)
    d = loadtxt(fn)
    
    v =  d[:,0]
    pdf = d[:,1]
    pdf = pdf / trapz(pdf, v)
    
    ix = pdf > 1e-3
    v = v[ix]
    pdf = pdf[ix]
    
    axpdf.semilogy(v, pdf, label='{} ms'.format(t))
    axpdf.set_title('velocity PDF')
    axpdf.set_ylabel('PDF')
    axpdf.set_xlabel('$v$ / std($v$)')

axpdf.legend(loc='best')
axpdf.grid('on')
fpdf.savefig('velocity_PDF.eps', bbox_inches='tight')

#spatial

files  = ["../mvx_165K_300mms_%dms.dat" % n for n in [1000, 2000, 4000]]
files2 = ["../tix_165K_300mms_%dms.dat" % n for n in [1000, 2000, 4000]]


lines = []
labels = []

f1, ax = plt.subplots(1,1)
f2, ax2 = plt.subplots(1,1)

NUM_COLORS = len(files)
cm = plt.get_cmap('hsv')
ax.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
ax2.set_prop_cycle('color', [cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

for (fn, fn2) in zip(files, files2):
    t = get_time(fn)
    d = loadtxt(fn)
    d2 = loadtxt(fn2)
    
    x = d[:,0]*pixd
    mvx = d[:,1]
    tix = d2[:,1]
    
    ax.plot(x, mvx, '-', label = '{} ms'.format(t))
    ax2.plot(x, tix, '-', label = '{} ms'.format(t))

ax.set_xlabel(r"$x / mm$"); ax2.set_xlabel(r"$x / mm$");
ax.set_ylabel(r"$v(x)$")
ax2.set_ylabel(r"$\sigma_v(x)$")
ax.legend(loc='best')
ax2.legend(loc='best')

f1.tight_layout()
f2.tight_layout()
f1.savefig('mvx.pdf')
f2.savefig('tix.pdf')

plt.show()
