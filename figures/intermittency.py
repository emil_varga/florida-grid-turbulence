# -*- coding: utf-8 -*-
"""
Created on Wed Sep 21 15:49:55 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt

import PRB_style

from data_util import get_files, get_temp


import itertools

plt.close('all')

vels = [50, 300]
times = [500, 1000, 2000, 4000, 8000, 16000]
ess_sf_order = 5

#vel = 300
#time = 4000
for vel in vels:
    for time in times:
        markers = itertools.cycle(('o', 's', '^', 'v', 'd', 'p'))

        files, ts, Ts = get_files('intermittency', T=[1.45, 1.65, 1.85, 2.0, 2.15], vel=vel, time=time)
        print(files)
        if len(files) == 0:
            continue

        fig_s3, ax_s3 = plt.subplots(1,1)
        ax_s3.set_xlabel("$x$ (mm)")
        ax_s3.set_ylabel("$S_3(x)/x$ (mm)")

        fig_ess_s3, ax_ess_s3 = plt.subplots(1,1)
        ax_ess_s3.set_xlabel('$S_3$ (a.u.)')
        ax_ess_s3.set_ylabel('$S_{}$ (a.u.)'.format(ess_sf_order))

        xmin = 0.2
        xmax = 4

        k=0

        zetas = {}
        offset=1
        ranges = {}
        for fn in files:
            d = np.loadtxt(fn)

            r = d[:,0]
            SF = abs(d[:,3])
            ax_s3.loglog(r, SF/r, label = '{:.2f} K'.format(get_temp(fn)), lw=3)

            Thandle = round(100*get_temp(fn))
            if not (Thandle in zetas):
                zetas[Thandle] = []
            for n in range(7):
                SFn = abs(d[:, n+1])
                SF3 = abs(d[:,3])
                ix = np.logical_and(r > xmin, r < xmax)
                p, cov = np.polyfit(np.log(SF3[ix]), np.log(SFn[ix]), deg=1, cov=True)
                zetas[Thandle].append((n+1, p[0], np.sqrt(cov[0,0])))
                if n+1 == ess_sf_order:
                    nix = np.logical_not(ix)
                    ax_ess_s3.loglog(SF3[nix], offset*SFn[nix], 'o', markersize = 3, color="0.5")
                    ax_ess_s3.loglog(SF3[ix], offset*SFn[ix], 'o', markersize = 6, markeredgecolor="none",
                               label = '{:.2f} K (exp: {:.2f})'.format(get_temp(fn), p[0]))
                    ax_ess_s3.loglog(SF3, offset*np.exp(np.polyval(p, np.log(SF3))), color="k")
                    offset *= 10
            k += 1

        ax_ess_s3.legend(loc='best')
        fig_ess_s3.savefig("S{}_S3_{}mms_{}ms.pdf".format(ess_sf_order, vel, time))

        ax_s3.legend(loc='best')
        ax_s3.axvspan(xmin=xmin, xmax=xmax, alpha=0.25)
        fig_s3.savefig("S3_{}mms_{}ms.pdf".format(vel, time))


        fig_zeta, ax_zeta = plt.subplots(1,1)
        ax_zeta.set_xlabel("$n$")
        ax_zeta.set_ylabel(r"$\zeta_n - n/3$")

        Ts = list(zetas.keys())
        Ts.sort()

        ax_zeta.axhline(0, color='black', lw=2)
        for T in Ts:
            sfz = np.array(zetas[T])
            np.savetxt('zeta_avg_{:.0f}K.dat'.format(T), sfz)
            ns = sfz[:,0]
            zeta = sfz[:,1]
            zeta_err = sfz[:,2]
            ax_zeta.errorbar(ns, zeta - ns/3, yerr=zeta_err, fmt='o', markersize=10,
                        label='{:.2f} K'.format(T/100.0), marker=next(markers))

        ns = np.arange(1, 10)
        ax_zeta.legend(loc='best')
        ax_zeta.set_xlim((0.5, 7.5))
        fig_zeta.savefig("zetan_{}mms_{}ms.pdf".format(vel, time))