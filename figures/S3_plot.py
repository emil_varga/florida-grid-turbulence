#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 22 11:04:09 2018

@author: emil

Plotting of third order structure function / Kolmogorov-4/5 law.
"""

import numpy as np
import matplotlib.pyplot as plt

import PRB_style

from data_util import get_files, get_time, get_temp, get_vel

import matplotlib

pixd = 11.2e-3 #mm, 11.2 um pixel distance

plt.close('all')

Ts   = [1.45, 1.65, 1.85, 2.0, 2.15]
vel = 50
time = '*'#[1000, 4000, 8000]

colors = {145: '-o',
          165: '-s',
          185: '-^',
          200: '-d',
          215: '-v'}

plt_sym = {145: '-o',
           165: '-s',
           185: '-^',
           200: '-d',
           215: '-v'}

for T in Ts:
    fsf, axsf = plt.subplots(1,1) #structure functions

    files, times, Ts = get_files('intermittency', T=T, vel=vel,
                                 time=time,
                                 no_subsets=True)

    for fn in files:
        print(fn)
        t = get_time(fn)
        TT = get_temp(fn)

        d = np.loadtxt(fn)

        x = d[:,0]
        S = d[:,3]

#        ix = np.logical_and(x>0.4, x<2.25)
#        p, V = np.polyfit(np.log(x[ix]), np.log(S[ix]), deg=1, cov=True)
#        times.append(t)

        axsf.loglog(x, S/x, colors[100*TT], label = '{:.1f} s'.format(t/1000),
                    ms = 6, mec = 'black', mew = 0.5, lw=1)
#        axsf.loglog(x, np.exp(p[1])*pow(x, p[0]), color='k')

    axsf.set_xlabel('$r$ (mm)')
    axsf.set_ylabel("$S_3/r$ (mm$^2$/s$^2$)")
    axsf.legend(loc='best')
    fsf.savefig('SF3_{:.0f}_{}mms.pdf'.format(100*T, vel))

files, times, Ts = get_files('intermittency', T='*', vel=vel,
                             time=4000,
                             no_subsets=True)

f3, ax3 = plt.subplots(1,1)
o = 1
for fn in files:
    print("at 4s: " + fn)
    T = get_temp(fn)
    d = np.loadtxt(fn)
    x = d[:,0]
    S = d[:,3]

    ax3.loglog(x, o*S/x, colors[100*T], label='{} K (x{})'.format(T, o),
               ms = 5, mec = 'black', mew = 0.25, lw=1)
    o *= 2

ax3.legend(loc='best')
ax3.set_xlabel('$r$ (mm)')
ax3.set_ylabel('$S_3(r) / r$ (mm$^2$/s$^3$)')
f3.savefig('K45_4s_{}mms.pdf'.format(vel))