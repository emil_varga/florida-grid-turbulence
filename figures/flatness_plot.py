#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 09:32:12 2018

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as path
import scipy.interpolate as intp

import data_dirs
import data_util

plt.close('all')

#
# plot the flatness using the kurtosis calculated from the velocity dumps
#

f, ax = plt.subplots(1,1)
ax.set_title('from samples')

for T in [1.45, 1.85, 2.00, 2.15]:
    files, times, Ts = data_util.get_files('kurtosis', T, vel=300, time=4000,
                                           force_pattern = 'ekurt*.dat')
    for fn in files:
        d = np.loadtxt(fn)
        pl = ax.plot(d[1:,0], d[1:,1], label=str(T))
        ax.axhline(d[0,1], ls='--', color=pl[-1].get_color())

ax.legend(loc='best')

#
# plot the flatness using the structure functions
#

f, ax = plt.subplots(1,1)
ax.set_title('from structure functions')

for T in [1.45, 1.65, 1.85, 2.00, 2.15]:
    files, times, Ts = data_util.get_files('intermittency', T, vel=300, time=4000)

    print(files)

    d = np.loadtxt(files[0])

    xs = d[:,0]
    SF2 = d[:,2]
    SF4 = d[:,4]

    ax.plot(xs, SF4/SF2**2, '-o', label = str(T))
ax.legend(loc='best')


#
# plot temperature-dependence cross-section
#

scale = 2
Ts = []
Fi = []
Fk = []

for T in [1.45, 1.65, 1.85, 2.00, 2.15]:
    filesi, _, _ = data_util.get_files('intermittency', T, vel=300, time=4000)
    filesk, _, _ = data_util.get_files('kurtosis', T, vel=300, time=4000,
                                           force_pattern = 'ekurt*.dat')

    Ts.append(T)

    di = np.loadtxt(filesi[0])
    Fii = intp.interp1d(di[:,0], di[:,4]/di[:,2]**2)
    Fi.append(Fii(scale))

    dk = np.loadtxt(filesk[0])
    Fki = intp.interp1d(dk[:,0], dk[:,1])
    Fk.append(Fki(scale))

f, ax = plt.subplots(1,1)
ax.set_title('temperature dependence')
ax.plot(Ts, np.array(Fi)-3, '-o', label='from structure functions')
ax.plot(Ts, Fk, '-x', label='from data set kurtosis')
ax.legend(loc='best')