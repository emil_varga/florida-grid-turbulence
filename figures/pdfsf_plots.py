#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 14:10:17 2018

@author: emil
"""

import PRB_style

import numpy as np
import data_dirs as dirs
import data_util as util

import matplotlib.pyplot as plt
from scipy.stats import linregress
import scipy.odr as odr

import os.path as path
from glob import glob

plt_sym = {145: '-o',
           165: '-s',
           185: '-^',
           200: '-d',
           215: '-v'}

plt.close('all')
flatness_fig, ax = plt.subplots(1,1)
ax.set_xlabel('scale (mm)')
ax.set_ylabel('flatness')

f_ess, ax_ess = plt.subplots(1,1)

f3, ax3 = plt.subplots(1,1)

fN, axN = plt.subplots(1,1)
axN.set_xlabel('$r$ (mm)');
axN.set_ylabel('number of samples')

o = 1
Ts = [1.45, 1.65, 1.85, 2.0, 2.15]
exps = [[] for n in range(7)]
exps_e = [[] for n in range(7)]
for T in Ts:
    files = util.get_files('sf_pdf', T, force_pattern='SF_nosub_T*_4000ms.dat',
                           just_files = True)
    fn_dump0 = path.join(dirs.get_base_data_dir(which='processed'), 'vinc_dumps',
                         'T{:.0f}'.format(100*T), '*300mms_4000ms*')
    fns = glob(fn_dump0)
    fns.sort(key=util.vinc_delta)
    d = np.loadtxt(fns[0])

    urms = np.std(d[:,-1])

    for fn in files:
        efn = fn.replace('SF', 'SFe')
        d = np.loadtxt(fn)
        de = np.loadtxt(efn)

        x = d[:,0]
        S = d[:, 1:-1]
        Se = de[:, 1:-1]
        N = d[:,-1]

        axN.plot(x, N, plt_sym[100*T], label="{:.2f} K".format(T),
                 ms = 6, mec = 'black', mew = 0.5, lw=1)

        F = S[:,3]/S[:,1]**2
        Fe = np.sqrt((Se[:,3]/S[:,1]**2)**2 +
                     (2*S[:,3]/S[:,1]**3*Se[:,1])**2)
        ax.errorbar(x, F, yerr=Fe, fmt='-o', label="{} K".format(T))

        ax3.errorbar(x, S[:,2]/x, yerr=Se[:,2], fmt='-o', label = '{:.2f} K'.format(T))

        ax_ess.errorbar(S[:,2], S[:,3]*o, xerr=Se[:,2], yerr=Se[:,3]*o, fmt='o',
                        label="{} K".format(T))
        ax_ess.set_xscale('log')
        ax_ess.set_yscale('log')

        fT, axT = plt.subplots(1,1)
#        axT.set_title('T = {} K'.format(T))

#        ix = np.logical_and(x > 0.34,
#                            x < 2.16)
        ix = np.logical_and(x > 0.2,
                            x < 4)
        for k in range(7):
            data = odr.RealData(np.log(S[ix,2]), np.log(S[ix,k]),
                                sx = Se[ix,2]/S[ix,2], sy = Se[ix,k]/S[ix,k])
            fit = odr.ODR(data, odr.unilinear)
            out = fit.run()
            axT.errorbar(data.x, data.y/urms**(k+1), xerr=data.sx, yerr=data.sy/urms**(k+1), fmt='o', capsize=5,
                         label=str(k+1))
            xs = np.linspace(data.x.min(), data.x.max(), 100)
            axT.loglog(xs, out.beta[0]*xs/urms**(k+1) + out.beta[1]/urms**(k+1), lw=2, color='k', zorder=100)
            exps[k].append(out.beta[0])
            exps_e[k].append(out.sd_beta[0])
#            exps_e[k].append(np.sqrt(out.cov_beta[0,0]))
#        axT.legend(loc='best')
        axT.set_xlabel('$S_3$')
        axT.set_ylabel('$S_n/v_{\mathtt{rms}}^n$')
        fT.tight_layout()
        fT.savefig('ess_T{:.0f}.pdf'.format(100*T))

    o *= 10

ax3.legend(loc='best')
ax3.set_xlabel('$r$ (mm)')
ax3.set_ylabel('$S_3$')
f3.tight_layout()
f3.savefig('S3.pdf')

ax.legend(loc='best')
flatness_fig.tight_layout()
flatness_fig.savefig('flatness.pdf')

ax_ess.legend(loc='best')

axN.legend(loc='best')
fN.tight_layout()
fN.savefig('nsamples.pdf')

#
# intermittency plot
# high-order structure function exponents
#

f, ax = plt.subplots(1,1)
axin = f.add_axes([0.25, 0.25, 0.4, 0.4])
ax.set_xlabel('$n$')
ax.set_ylabel('$\zeta_n - n/3$')

exps = np.array(exps)
exps_e = np.array(exps_e)
ks = np.arange(7) + 1
for (T, n) in zip(Ts, range(len(Ts))):
    ax.errorbar(ks, exps[:, n] - ks/3, yerr=exps_e[:,n], fmt=plt_sym[100*T], capsize=5,
                ms = 4, mec = 'black', mew = 0.75, lw=1)
    d = np.loadtxt('zeta_avg_{:.0f}K.dat'.format(100*T))
    axin.errorbar(d[:,0], d[:,1] - d[:,0]/3, yerr = d[:,2], fmt=plt_sym[100*T], capsize=5,
                  ms = 4, mec = 'black', mew = 0.75, lw=1)
ax.plot(ks, ks*0, '--')
axin.plot(ks, ks*0, '--')
#ax.legend(loc='best')

f.tight_layout()
f.savefig('zetas_pdf.pdf')