# -*- coding: utf-8 -*-
"""
Created on Wed May  3 22:37:32 2017

plots the PDFs of velocity increments calculated from the velocity
increment dumps (vinc_dump).

PDF is calculated either as a histrogram or KDE

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as optim
from scipy.stats import gaussian_kde, kurtosis
import data_util
from data_util import vinc_delta
import os.path as path
from scipy.special import erf

import scipy.interpolate as intp
from functools import partial

import statsmodels.api as sm

import figures.PRB_style


#test fitting functions
def log_gauss(x, s, mu):
    return -0.5*np.log(2*np.pi*s**2) - 0.5/s**2*(x-mu)**2
def gauss(x, s, mu):
    return np.exp(-(x-mu)**2/2/s**2)/np.sqrt(2*np.pi*s**2)
def mordant(x, s, m):
    a = np.exp(s**2/2)/4/m
    z = 1 - erf((np.log(abs(x/m)) + s**2)/np.sqrt(2)/s)

    return a*z

class PDF(object):
    """
    Class encapsulating calculation and handling of
    probability distribution functions.
    """
    def __init__(self, vs, labels = None, normalize=True, center=True,
                 filter_outliers=False):
        """
        Initializes the structure to data given by vs (1D array or a filename)
        and any additional metadata with labels (i.e., sepration for velocity increments)

        normalize=True renormalizes the PDF to std=1
        center=True shifts the dataset to zero mean
        """
        self.labels = labels

        if isinstance(vs, str):
            data = np.loadtxt(vs)
            self.data = data[:,2]
        else:
            self.data = vs

        if filter_outliers:
            s = self.data.std()
            m = self.data.mean()
            ix = (np.abs(self.data) - m) < 4*s
            self.data = self.data[ix]

        if normalize:
            std = np.std(self.data)
            self.data = self.data/std
        if center:
            self.data = self.data - np.mean(self.data)

        #number of samples
        self.N = len(self.data)

        #the support
        self.xx = np.linspace(5*self.data.min(), 5*self.data.max(), 500)

        #create the PDF estimates
        self.make_pdf_hist()
        self.make_pdf_kde()

        self.pdfs = {'histogram' : self.ihpdf,
                     'kde'       : self.ikpdf}

        self.hgp, _ = optim.curve_fit(gauss, *self.get_histogram())
        self.hmp, _ = optim.curve_fit(mordant, *self.get_histogram(),
                                      p0 = [0.4, self.data.std()])

        self.kgp, _ = optim.curve_fit(gauss, *self.get_kde())
        self.kmp, _ = optim.curve_fit(mordant, *self.get_kde(restrict = (lambda x: abs(x) > 2*self.data.std())),
                                      p0 = [0.4, self.data.std()])
#        self.kmp = [0.44, 1.1*np.std(self.data)]

        self.models_and_params = {'histogram' : {'gauss' : (gauss, self.hgp),
                                                 'mordant' : (mordant, self.hmp)},
                                  'kde'       : {'gauss' : (gauss, self.kgp),
                                                 'mordant' : (mordant, self.kmp)}}

    def get_support(self, restrict=False):
        """
        Return a linspace array enveloping the data range.
        With restrict = True will return support only within the data range.
        restrict = 'tail' will limit the support to outside the data range
        restrict can also be a callable accepting 1-d array of float and returning
        1-d array of bool
        """

        if restrict:
            ix = np.logical_and(self.xx > self.data.min(),
                                self.xx < self.data.max())
            if restrict == 'tail':
                ix = np.logical_not(ix)
            elif callable(restrict):
                ix = restrict(self.xx)
            return self.xx[ix]
        return self.xx

    def make_pdf_kde(self, adjust=2):
        kernel = sm.nonparametric.KDEUnivariate(self.data)
        kernel.fit(adjust=adjust)
        self.kpdf = kernel.evaluate(self.xx)
        norm = np.trapz(self.kpdf, self.xx)

        self.ikpdf = intp.interp1d(self.xx, self.kpdf/norm, fill_value=0, bounds_error=False)

    def make_pdf_hist(self, bins='auto'):
        self.h_vals, bins = np.histogram(self.data, bins=bins, density=True)
        self.cents = 0.5*(bins[:-1] + bins[1:])
        norm = np.trapz(self.h_vals, self.cents)

        self.ihpdf = intp.interp1d(self.cents, self.h_vals/norm, fill_value=0, bounds_error=False)

    def get_histogram(self):
        return self.cents, self.h_vals

    def get_kde(self, restrict=True):
        x = self.get_support(restrict)

        return x, self.ikpdf(x)

    def get_fit(self, which_pdf, which_model, xs=None):
        if xs is None: xs = self.xx
        model, params = self.models_and_params[which_pdf][which_model]
        return xs, model(xs, *params)

    def get_moment_dist(self, n, which_pdf, use_fit=False, upper_bound=False, range_restrict=False,
                        plain_pdf = False):
        x = self.get_support(restrict=range_restrict)

        if not upper_bound:
            if not use_fit:
                pdf = self.pdfs[which_pdf]
            else:
                pdf = lambda x: self.get_fit(which_pdf, use_fit, x)[1]
        else:
            epdf = self.pdfs[which_pdf]
            gpdf, gp = self.models_and_params[which_pdf]['gauss']
            kpdf, kp = self.models_and_params[which_pdf]['mordant']

            xf = self.get_support()

            def nnpdf(x):
                return np.max(np.column_stack((epdf(x),
                                               gpdf(x, *gp),
                                               kpdf(x, *kp))), axis=1)
            def pdf(x):
                return nnpdf(x) / np.trapz(nnpdf(xf), xf)

        if plain_pdf:
            return x, pdf(x)
        return x, x**n*pdf(x)

    def get_moment(self, n, which_pdf, use_fit=False, upper_bound=False, range_restrict=False):
        x, pdf = self.get_moment_dist(n, which_pdf, use_fit, upper_bound, range_restrict,
                                      plain_pdf = True)
        xfull, pdffull = self.get_moment_dist(n, which_pdf, use_fit, upper_bound, range_restrict=False,
                                              plain_pdf = True)
        apdf = np.abs(x**n)*pdf / np.trapz(pdffull, xfull)

        if range_restrict == 'tail':
            #the support is not continuous when looking only on tails
            pix = x > 0
            nix = x < 0
            out = np.trapz(apdf[pix], x[pix]) + np.trapz(apdf[nix], x[nix])
            return out

        return np.trapz(apdf, x)


    def __call__(self, xs=None, which='kde', fit_upper_bound=False):
        #just get the support if xs is not specified
        if xs is None: xs = self.xx

        if which == 'kde':
            return self.ikpdf(xs)
        elif which == 'histogram':
            return self.ihpdf(xs)
        else:
            raise ValueError('Unrecognized PDF type {}, accepted values are kde and histogram'.format(which))


if __name__ == '__main__':
    plt.close('all')

    #order of the structure function
    p = 6

    #get the data dumps
    files, _, _ = data_util.get_files('vinc_dumps', 1.45, 300, 4000)
    files.sort(key=vinc_delta)
    #files = files[::2]

    files = files[15:16]
    for fn in files:
        print(path.split(fn)[-1], vinc_delta(fn))

    #histrogram plot
    ff, axx = plt.subplots(1,1)
    axx.set_title('PDF -- histogram')

    #"p-th" moment histrogram val^p*PDF(val)
    ff, axx2 = plt.subplots(1,1)
    axx2.set_title('$v^{}$PDF(v) -- histrogram'.format(p))

    kde_fig, ax = plt.subplots(1,1)
#    ax.set_title('PDF -- KDE')
    ax.set_xlabel(r"$\delta v(r) / \left\langle \delta v(r)^2\right\rangle^{1/2}$")
    ax.set_ylabel(r"PDF$_r$")
    ax.set_xlim((-7, 6))
    ax.set_ylim((1e-6, 1))

    moment_fig, ax2 = plt.subplots(1,1)
#    ax2.set_title('$v^{}$PDF(v) -- KDE'.format(p))
    ax2.set_xlabel(r"$\delta v(r) / \left\langle \delta v(r)^2\right\rangle^{1/2}$")
    ax2.set_ylabel("$\\delta v(t)^{}$PDF$_r$".format(p))
    ax2.set_xlim((-10, 10))

    cmap = plt.get_cmap('jet')

    o = 0
    o_r = 1
    for file, n in zip(files, range(len(files))):
        pdf = PDF(file, normalize=True)

        Sn = pdf.get_moment(p, which_pdf='kde')
        print("{} S2= {} ({})".format(path.split(file)[-1],Sn, pdf.N))

        #plot the histogram and the gaussian fit
        axx.semilogy(*pdf.get_histogram(), '-o', label=path.split(file)[-1])
        axx.semilogy(*pdf.get_fit('histogram', 'gauss'))
        axx.semilogy(*pdf.get_fit('histogram', 'mordant'))
        axx.semilogy(*pdf.get_kde(), '--', color='red')
        axx2.plot(*pdf.get_moment_dist(p, 'histogram'), 'o')
        axx2.plot(*pdf.get_moment_dist(p, 'histogram', use_fit='gauss'), '--')
        axx2.plot(*pdf.get_moment_dist(p, 'histogram', use_fit='mordant'), '-.')

        pl = ax.semilogy(*pdf.get_kde(), '-o', lw=1, ms = 3, mec='black', mew='0.75')
        ax.semilogy(*pdf.get_fit(which_pdf='kde', which_model='gauss'), label='Gaussian')
        ax.semilogy(*pdf.get_fit(which_pdf='kde', which_model='mordant'), '--', label='Heavy-tailed')

        ax.plot(pdf.data, np.full_like(pdf.data, o_r*1e-6), '|', color=pl[-1].get_color(), ms=8)
#        o_r *= 1.1

        x, mpdf= pdf.get_moment_dist(p, 'kde', upper_bound = True)
        xt, tail = pdf.get_moment_dist(p, 'kde', upper_bound = True, range_restrict='tail')

        xkde, pkde = pdf.get_moment_dist(p, 'kde')
        ax2.plot(xkde, pkde+6, '-', label='KDE')

        xgauss, pgauss = pdf.get_moment_dist(p, 'kde', use_fit='gauss')
        ax2.plot(xgauss, pgauss + 4, '--', label='Gaussian')

        xmord, pmord = pdf.get_moment_dist(p, 'kde', use_fit='mordant')
        ax2.plot(xmord, pmord + 2, '-.', label='Heavy-tailed')

        pp = ax2.plot(x, mpdf + o, '-', label='re-normalized', zorder=-100, lw=2)
        ax2.fill_between(xt, o, tail + o, color=pp[-1].get_color(), alpha=0.5, where = xt < 0)
        ax2.fill_between(xt, o, tail + o, color=pp[-1].get_color(), alpha=0.5, where = xt > 0)
        ax2.axhline(o, color=pp[-1].get_color(), zorder=-100)
#        o += 10

    ax.legend(loc='best')
    ax2.legend(loc=(0.66, 0.7))

    kde_fig.tight_layout()
    kde_fig.savefig('pdf_kde.pdf')

    moment_fig.tight_layout()
    moment_fig.savefig('pdf_moment.pdf')