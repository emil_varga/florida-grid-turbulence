# -*- coding: utf-8 -*-
"""
Created on Wed Oct 12 12:26:05 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import os.path as path
from glob import glob
from parse import parse
import scipy.interpolate as interp
import re
import tcal

def sweep_to_timeseries(t, X, sweeptime, method):
    lw = t.min()
    tout = []
    xout = []
    while lw < t.max():
        ix = np.logical_and(t >= lw, t < lw + sweeptime)
        tt = t[ix]
        xx = X[ix]
        if method == 'max':
            I = xx.argmax()
            tout.append(tt[I])
            xout.append(xx[I])
        elif method == 'mean':
            tout.append(tt.mean())
            xout.append(xx.mean())
        else:
            raise ValueError("only 'max' and 'mean' methods are supported")
        
        lw += sweeptime
    return np.array(tout), np.array(xout)

def filename_to_id(fn):
    '''The filenames are in the form
            SS_sweep_<date>_<time>.dat
        
        This function returns a number representing
        the date and time which is a unique identifier
        of the file.
    '''
    tok = re.split('[_.]', fn)
    time_id = int(tok[-3] + tok[-2])
    return time_id

def crunch_dir(data_dir, B, D, background_thr = 8000, sweep_time=0.1):
    filename_pattern = 'SS_*.dat'
    data_fns = glob(path.join(data_dir, filename_pattern))
    data_fns.sort(key=filename_to_id)
#    print(data_fns)
    
    png_filename_pattern = 'SS_*.png'
    png_files = glob(path.join(data_dir, 'GOOD',
                               png_filename_pattern))
    good_ids = [filename_to_id(fn) for fn in png_files]
#    print(good_ids)
    
    background = None
    tb         = None
    L          = None
    t          = None
    y_mean     = None
    count      = 0
    
    kappa = 0.997e-3
    n = 0
    for fn in data_fns:
        n += 1
        print("{} : {}/{}".format(fn, n, len(data_fns)))
        if filename_to_id(fn) not in good_ids:
            print('bad file, skipping...')
            continue
    
        with open(fn) as file:
            header = file.readline()
        tok = parse("#frequency:{}\tphase:{}\tsweep{}", header)
        freq_range = eval(tok[2])
        
        d = np.loadtxt(fn)
        
        if max(freq_range) < background_thr:
            #we measured background here, save it for next couple of files
            tb_raw, background_raw = sweep_to_timeseries(d[:,0], d[:,2],
                                                         sweeptime = sweep_time,
                                                         method = 'mean')
            if tb is None and background is None:
                tb = tb_raw
                background = background_raw
            else:
                background = interp.interp1d(tb_raw, background_raw,
                                             kind='linear',
                                             fill_value='extrapolate')(tb)
            if t is None and L is None:
                t = tb
                L = np.zeros_like(background)
                y_mean = np.zeros_like(background)
            continue;
        
        traw, yraw = sweep_to_timeseries(d[:,0], d[:,2], sweeptime=sweep_time, method='max')
        
        y = interp.interp1d(traw, yraw, kind='linear', fill_value='extrapolate')(t)
        y = y - background
        y_mean += y
        y0 = y[t > t.max() - 1].mean()
        L += 6*np.pi*D/B/kappa*(y0/y - 1)
        count += 1
    
    L/=count
    y_mean/=count
    
    return t, L, y_mean
    
def process_dir(T, v, D, background_thr, t0, basedir='../data/', sweep_time=0.1, dir_suffix = None):
    dirname = path.join(basedir, 'T{:.0f}'.format(T*100), '{:d}mms'.format(v), 'second-sound')
    if dir_suffix is not None:
        dirname += dir_suffix
    print(dirname)
    
    t, L, y_mean = crunch_dir(dirname, tcal.B(T), D, background_thr, sweep_time=sweep_time)

    filename = '../L_{:.0f}K_{:d}mms.dat'.format(100*T, v)
    if dir_suffix is not None:
        filename.replace('.dat', dir_suffix+'.dat')
    np.savetxt(filename, np.column_stack((t-t0, L, y_mean)))
    f, ax = plt.subplots(1,1)
    ax.loglog(t-t0, L, '-o')
    f.suptitle('{:.2f} K, {:d}mm/s'.format(T, v))
    f.savefig(filename.replace('dat', 'pdf'))


process_dir(1.45, v=50, D=79, background_thr=7600, t0=20)
process_dir(1.45, v=300, D=79, background_thr=7600, t0=20)
process_dir(1.65, v=50, D=32, background_thr=7800, t0=20, dir_suffix = '_0')
process_dir(1.65, v=50, D=28, background_thr=7800, t0=20, dir_suffix = '_1')
process_dir(1.65, v=300, D=32, background_thr=7800, t0=30)
process_dir(1.85, v=50, D=60, background_thr=8600, t0=20)
process_dir(1.85, v=300, D=67, background_thr=13000, t0=20)
process_dir(2.00, v=300, D=48, background_thr=9500, t0=30)

#process_dir(2.00, v=50, D=41, background_thr=12700, t0=30)

#t, L, y_mean = crunch_dir('../data/T185/50mms/second-sound/',
#                          background_thr = 8550)
#np.savetxt('../T185_50mms_L.dat', np.column_stack((t, L, y_mean)))
#f, ax = plt.subplots(1,1)
#ax.loglog(t-20, L, '-o')
#f.suptitle('1.85K, 50mm/s')
#f.savefig('../T185_50mms_L.pdf')
#
#t, L, y_mean = crunch_dir('../data/T185/300mms/second-sound/',
#                          background_thr = 13000)
#np.savetxt('../T185_300mms_L.dat', np.column_stack((t, L, y_mean)))
#f, ax = plt.subplots(1,1)
#ax.loglog(t-20, L, '-o')
#f.suptitle('1.85K, 300mm/s')
#f.savefig('../T185_300mms_L.pdf')