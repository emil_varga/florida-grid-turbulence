# -*- coding: utf-8 -*-
"""
Created on Tue May  2 22:22:22 2017

dumps the velocity increment samples into a single file
which is used for calculating PDFs

@author: emil
"""

import os
import os.path as path
import sys
import re
from multiprocessing import Pool
from glob import glob
import numpy as np
from functools import partial

import scipy.interpolate as interp
from TracerLine import TracerLine

import data_util as util
import data_dirs

pixd = 11.2e-3 #mm, 11.2 um pixel distance

def process_dir(dname, delta, badimg = None, use_old=True, allow_mv = True,
                reference=None, max_points_per_file=256):

    img_index = lambda fn: int(re.split('[_.]', fn)[-2])

    imgfs = glob(path.join(dname, "LHe*.dat"))
    imgfs.sort(key=img_index)
    T_d, v_d, t_d, good_img_idx, base_img_idx = util.dname_data(dname)
    tdrift, idx = util.fname_data(imgfs[0])

    t_dec = t_d

    is_baseline = False

    multi = re.search(r"\d+ms_\d+", dname)
    if multi is None:
        basename = "%dK_%dmms_%dms" % (T_d*100, v_d, t_dec)
    else:
        basename = "%dK_%dmms_%s" % (T_d*100, v_d, multi.group())

    print("MULTI = {}, BASENAME = {}".format(multi, basename))

    if allow_mv:
        dname_out = 'vinc_dumps_prof'
    else:
        dname_out = 'vinc_dumps'

    out_dir = path.join(data_dirs.get_base_data_dir(which='processed'),
                        dname_out,
                        "T{:.0f}".format(100*T_d))
    os.makedirs(out_dir, exist_ok=True)

    DUMP_filename = "DUMP_"+basename+"_delta_"+str(delta).replace('.', 'p')+".dat"
    DUMP_filename = path.join(out_dir, DUMP_filename)
    DUMP_file = open(DUMP_filename, 'w')

    print('*************************************')
    print('Processing dir ' + dname)
    print('Creating dump file ' + DUMP_filename)
    print('*************************************')

    #get the mean profile
    print(T_d)
    if allow_mv:
        fmvx, times, Ts = util.get_files("velocity-profile", vel=v_d, time=t_dec, T=T_d)
        print(fmvx, times, Ts)
        mvx = np.loadtxt(fmvx[0])
        mv_prof = interp.interp1d(mvx[:,0], mvx[:,1], kind='linear', bounds_error=False)
        print('Using mean velocity profile {} for {}'.format(fmvx[0], dname))
    else:
        mv_prof = None

    Nxbase = 512
    x_base = np.linspace(0, 1024, num=Nxbase)

    NN = len(imgfs)

    if badimg is not None:
        bad_idx = np.loadtxt(badimg, dtype=int)
    else:
        bad_idx = []

    for fn in imgfs[:NN]:
        idx = img_index(fn)
        print("{} : {}/{}, delta={}".format(dname, idx, NN, delta))
        if not (idx in good_img_idx) or (idx in bad_idx):
            print('bad image, skipping...')
            continue;
        sys.stdout.flush()

        t_drf, fidx = util.fname_data(fn)
        if t_drf != tdrift or fidx != idx:
            raise ValueError('mismatch in file data! {} {} {}'.format(fn, tdrift, idx))

        if idx in base_img_idx:
            is_baseline=True
        else:
            is_baseline=False

        tl = TracerLine(fn, use_old=use_old, baseline=False, use_intensity=True)

        try:
            tl.find_ridge(kde_bw = 10, fancy_sigmas=False,
                          elastic=False, cleanup=True)
        except:
            print('Error in {}'.format(fn))
            raise

        line = tl.line_x

        if(is_baseline):
            baseline = line
        else:
            #calculate the velocities
            v_y = (baseline(x_base) - line(x_base))*pixd/tdrift*1000
            if len(v_y) != len(x_base):
                print("ERROR {}".format(fn))
                sys.exit(-1)

            #get rid of nans
            ix = np.logical_not(np.isnan(v_y))
            xsc = x_base[ix] #"clean" data
            vyc = v_y[ix]

            #cut the line to 600 pix around the centre
            mid = 0.5*(xsc.min() + xsc.max())
            ix_c = np.logical_and(xsc > (mid - 300),
                                  xsc < (mid + 300))
            xsc = xsc[ix_c]
            vyc = vyc[ix_c]

            ix = np.arange(len(ix))[ix][ix_c]


            #subtract the mean profile if we are using it
            if mv_prof is not None:
                vyc = vyc - mv_prof(xsc)
                ix= np.logical_not(np.isnan(vyc))
                xsc = xsc[ix]
                vyc = vyc[ix]

            vyc = vyc - vyc.mean()

            vel_x = interp.interp1d(xsc, vyc, kind='linear')

            total = max(xsc) - min(xsc)
            if delta > 1e-5:
                #calculate structure function
                x0 = min(xsc)
                while x0 + delta/pixd < max(xsc):
                    sf0 = vel_x(x0+delta/pixd) - vel_x(x0)
                    DUMP_file.write("{}\t{}\t{}\n".format(idx, x0, sf0))
                    x0 += total/max_points_per_file
            else:
                for x0 in xsc:
                    sf0 = vel_x(x0)
                    DUMP_file.write("{}\t{}\t{}\n".format(idx, x0, sf0))

    DUMP_file.close()

if __name__ == "__main__":
    data = { 2.15 : {300 : [1000, 4000]},
             2.00 : {300 : [500, 1000, 4000],
                     50  : [500, 2000]},
             1.85 : {300 : [1000, 2000, 4000, 8000],
                     50  : [1000, 2000, 4000, 8000],
                     0   : [0]},
             1.65 : {300 : [1000, 2000, 4000, 8000, 16000],
                     50  : [1000, 2000, 4000, 8000]},
             1.45 : {300 : [1000, 2000, 4000, 8000],
                     50  : [1000, 2000, 4000, 8000]}}
    base_dir = data_dirs.get_base_data_dir(which='data')
    dirs = []
    for T in data:
        for v in data[T]:
            for t in data[T][v]:
                dirs += glob(path.join(base_dir, 'T{:.0f}/{}mms/images/{}ms*'.format(100*T, v, t)))
    print('Found directories ({}):'.format(len(dirs)))
    for d in dirs:
        print("\t"+d)

    def sort_key(dn):
        T, v, t, _, _ = util.dname_data(dn)
        return t + 1e4*v + 1e8*T
    dirs.sort(key=sort_key)

    deltas = [0.1, 0.2, 0.5, 1, 2, 3, 4, 5, 6]
    deltas += [0.15, 0.3, 0.4, 0.6, 0.8, 0.9,
              1.2, 1.4, 1.6, 1.8,
              2.2, 2.4, 2.6, 2.8,
              3.2, 3.4, 3.6, 3.8]
    deltas += [0]

    for delta in deltas:
        with Pool(6) as p: #first without mean subtraction
            p.map(partial(process_dir, delta=delta, allow_mv = False), dirs)
    for delta in deltas:
        with Pool(6) as p: #the second time with mean subtraction
            p.map(partial(process_dir, delta=delta, allow_mv = True), dirs)