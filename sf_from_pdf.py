#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 23:01:50 2018

This estimates the probability density function from measured samples
using either a histogram, KDE or a fit of a prescribed function

Then uses this estimated PDF to calculate structure functions of arbitrary
order and estimates the error from extrapolation of the PDF

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import data_util
import data_dirs
import os.path as path
import os

from histogram import PDF

if __name__ == '__main__':
    Ts = [1.45, 1.65, 1.85, 2.0, 2.15]
    times = ['*{}ms*'.format(t) for t in [500, 1000, 2000, 4000, 8000, 16000]]
    for T in Ts:
        files, _, _ = data_util.get_files('vinc_dumps', T, 300, time=4000,
                                          force_pattern='*4000ms*')

        files.sort(key=data_util.vinc_delta)

        deltas = []
        S = [([], []) for n in range(7)]
        Ns = []

        k = 0
        for fn in files:
            k += 1
            print(fn)
            pdf = PDF(fn, normalize=False)

            Ns.append(pdf.N)
            deltas.append(data_util.vinc_delta(fn))

            for n in range(7):
                Snub = pdf.get_moment(n+1, 'kde', upper_bound=False)
                Sub  = pdf.get_moment(n+1, 'kde', upper_bound=True)
                S[n][0].append(Snub) #structure function
                S[n][1].append(np.abs(Snub - Sub))
#                S[n][1].append(pdf.get_moment(n+1, 'kde', upper_bound=True, range_restrict='tail')) #error

        S = np.array(S)

        sf_out = np.column_stack((deltas, S[:,0,:].T, Ns))
        sfe_out = np.column_stack((deltas, S[:,1,:].T, Ns))

        dir_out = path.join(data_dirs.get_base_data_dir(which='processed'),
                            'sf_pdf', 'T{:.0f}'.format(100*T))
        os.makedirs(dir_out, exist_ok=True)

        sf_file_out = path.join(dir_out, "SF_nosub_T{:.0f}_4000ms.dat".format(100*T))
        sfe_file_out = path.join(dir_out, "SFe_nosub_T{:.0f}_4000ms.dat".format(100*T))

        np.savetxt(sf_file_out, sf_out,
                   header = "delta(mm)\tSFn\t#points")
        np.savetxt(sfe_file_out, sfe_out,
                   header = "delta(mm)\tSFen\t#points")

        plt.close('all')

        f, ax = plt.subplots(1,1)
        ax.set_title('number of points')
        ax.plot(deltas, Ns, '-o')

        f, ax = plt.subplots(1,1)
        ax.errorbar(S[2][0], S[3][0], xerr=S[2][1], yerr=S[3][1], fmt='o')
        ax.set_xlabel('S3')
        ax.set_ylabel('S4')

        S4 = S[3][0]; S4e = S[3][1]
        S2 = S[1][0]; S2e = S[1][1]
        f, ax = plt.subplots(1,1)
#        ax.plot(deltas, S4/S2**2, '-o')
        ax.errorbar(deltas, S4/S2**2,
                    yerr = np.sqrt((S4e/S2**2)**2 + (2*S4/S2**3*S2e)**2),
                    fmt='o')