# -*- coding: utf-8 -*-
"""
Created on Tue Nov  8 14:32:24 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import scipy.optimize as optim
import scipy.interpolate as interp
import scipy.integrate as integ
from data_util import get_files
import tcal
T = 1.65
kappa = 0.997e-3 #cm^2/s
D = 0.95 #cm, channel width
C = 1.4 # Kolmogorov constant
B = tcal.B(T)
rhos = tcal.rs(T)
rho  = tcal.r(T)

#plt.close('all')

d = np.loadtxt('../processed/second-sound/L_{:.0f}K_300mms.dat'.format(100*T))
files, times, _ = get_files('turbulence-intensity-profile', T=T, vel=300)
times = np.array(times)/1000.0

t0 = 0
ix = np.logical_and(d[:,0] > t0, d[:,1] > 0)
t = d[ix,0] - t0
L = d[ix,1]
#a = d[ix,2]

u2 = []
for fn in files:
    ti = np.loadtxt(fn)
    u2.append((ti[:,1].mean()/10)**2)
u2 = np.array(u2)

ix = times >= 1
times = times[ix]
u2 = u2[ix]

tmin = min(times)
tmax = max(times)

f, (axL, axU2) = plt.subplots(1, 2, sharex=True)
axL.loglog(t, L)
axU2.loglog(times, u2, '-o')

ix = np.logical_and(t >= tmin-0.05, t <= tmax+2*np.diff(t).mean())
trange = t[ix]
Lrange = L[ix]

E = 1.5*u2
dE = E - E[0]

dE_L = B*kappa**2*rhos/rho*(Lrange[0] - Lrange)
I = integ.cumtrapz(Lrange**2, trange, initial=0)
II2 = integ.cumtrapz(I**2, trange, initial=0)
print(times, min(trange), max(trange))
dE_Lp = interp.interp1d(trange, dE_L)(times)
Ip = interp.interp1d(trange, I)(times)
II2p = interp.interp1d(trange, I**2)(times)

nu = np.sum(Ip*(dE_Lp - dE))/kappa**2/np.sum(II2p)
print(nu/kappa)
#nu = 0.33*kappa

f, ax = plt.subplots(1,1)
ax.loglog(times, E[0] + dE, 'o')
ax.loglog(trange, E[0] + dE_L - nu*kappa**2*I)
ax.set_xlabel("$t$ (s)")
ax.set_ylabel("$E$ (cm$^2$/s^$2$)")
ax.set_xlim((0.9, 20))
f.tight_layout()
f.savefig("E_L_decay.pdf")