# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 13:15:57 2016

@author: emil
"""

import numpy as np
import matplotlib.pyplot as plt

import os.path as path
from glob import glob

file = 'spectrum-phase20p75-150VDC-_7500.0Hz-9000.0Hz_2016-10-18_11-27-19_0.csv'
data_dir = '../data/T165/300mms/second-sound/preliminary'
#files = glob(path.join(data_dir, '*.csv'))
#files.sort()
#print(files)
#fn = files[10]
#print(fn)

fn = path.join(data_dir, file)

d = np.loadtxt(fn, delimiter=',')

f, ax = plt.subplots(1,1)

ax.plot(d[:,0], d[:,2])
ax.plot(d[:,0], d[:,1] - d[:,1].mean(), 'r')

xmax = np.argmax(d[:,1][5:]) + 5
xmin = np.argmin(d[:,1][5:]) + 5

ax.axvline(d[xmin,0], color='k')
ax.axvline(d[xmax,0], color='k')

ix = np.logical_and(d[:,0] > 7700,
                    d[:,0] < 7800)
b = d[ix,2].mean()
ax.axhline((d[:,2].max() - b)/2 + b)

print("Width = {}".format(abs(d[xmin,0] - d[xmax,0])))
plt.show()