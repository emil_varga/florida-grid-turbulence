# -*- coding: utf-8 -*-
"""
Created on Wed Nov  9 15:44:30 2016

@author: emil
"""

import numpy as np
from glob import glob
import matplotlib.pyplot as plt

file_pat = '../mean_Ek_165K_300mms_4000ms'

files = glob(file_pat+"_*.dat")
file_out = file_pat + '.dat'

d_out = 0
total_counts = 0
f, ax = plt.subplots(1,1)
for fn in files:
    d = np.loadtxt(fn)
#    ix = np.logical_and(d[:,0] > 1.77,
#                        d[:,0] < 8.16)
    counts = d[:,-1]
    d_out += d[:,1]*counts
    total_counts += counts
    ax.loglog(d[:,0], d[:,1])

d_out /= total_counts
d_out = np.column_stack((d[:,0], d_out, total_counts))
ax.loglog(d_out[:,0], d_out[:,1])

np.savetxt(file_out, d_out)