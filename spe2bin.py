# -*- coding: utf-8 -*-
"""
Created on Wed Oct 01 13:45:31 2014

spe2bin is a small set of tools for working with SPE files in Python. This is
adapted from the Roper_ascii_to_mat_bin.m routine available at:

ftp://ftp.princetoninstruments.com/Public/Software/User%20Written%20Utilities/matlab/

This also includes a function to properly repair frames of data that get
corrupted by an "off by 1" error that shows up when using some of the old PCI
cards.

Functions:
read_datafile(filename):
    A function to read a SPE file, decode the header and return a properly 
    shaped array. 
    
    Input: A SPE file. 
    Output: A numpy.ndarray
  

@author: Cantwell Carson
carsonc@gmail.com


-----------------------------------------

most of the stuff cut down from the orignal file and modified slighly
(start reading data from word 2050, not 1025) to work with PI-MAX

Emil Varga
varga.emil@gmail.com
"""



import numpy as np

def read_datafile(filename):
    """
    A function to read a SPE file, decode the header and return a properly 
    shaped array. 
    
    Input: A SPE file. 
    Output: A numpy.ndarray
    """
    
    with open(filename, 'rb') as fid:
        hdr = np.fromfile(fid, np.uint16, 2050)  # 2050 uint16 = 4100 bytes 
                                                 #             = 32800 bits
                                                 #             = 1024 vals

    Xdim, Ydim, Zdim, DataType = hdr[21], hdr[328], hdr[723], hdr[54]
    # remember that the Decimal byte offset is 2x the read value,
    # so hdr[21] = DBO[42], (42 / 2) = 21
                    
    with open(filename, 'rb') as fid:
            ImMat = np.fromfile(fid, np.uint16)[2050:]    
#    print(Xdim, Ydim, Zdim, DataType, ImMat.size)
    a = np.reshape(ImMat,[Xdim,Ydim])
    return a
