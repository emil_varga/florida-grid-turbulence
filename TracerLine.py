# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 15:37:19 2016

@author: emil
"""

import numpy as np
from numpy.linalg import norm, eigh
import scipy.sparse as sparse
import scipy.spatial as spt
import scipy.interpolate as interp
import scipy.optimize as opt
import scipy.odr as odr
from scipy.stats import gaussian_kde
from scipy.ndimage.filters import gaussian_filter

import matplotlib.pyplot as plt

import os.path as path
from gauss_line_kde import gauss_line_kde
import data_dirs

def get_sigmas(nnd):
    rmin = nnd.min()
    rmax = nnd.max()

    rs = np.linspace(rmin, rmax, 128)
    Ns = np.zeros_like(rs)
    for k in range(rs.size):
        Ns[k] = sum(nnd < rs[k])

    p = np.polyfit(rs, Ns, deg=2)
    return 2*np.pi/np.sqrt(abs(p[0]))


def gauss(x, *p):
    b, A, mu, s = p
    return b + A*np.exp(-(x-mu)**2/(2.*s**2))

class TracerLine:
    def reload(self):
        self.data = np.loadtxt(self.filename)
        self.data = self.data[:,1:1025]

        dsp = sparse.coo_matrix(self.data)
        self.x0 = dsp.col; self.y0 = dsp.row
        self.dat0 = dsp.data
        self.N0 = self.x0.size
        self.filtered = False

        self.line_points      = None
        self.line_points_init = None
        self.x       = None
        self.y       = None
        self.dat     = None
        self.weights = None
        self.spdf = None

        #these return y as a function of x
        self.line_x  = None #line as a function of x
        self.line_xn = None #line as a function of x
                            #normalized to [0,1]
        #these return (x,y) as a function of arc length
        self.line_l  = None #line as a function of arc length
        self.line_ln = None #line as a function of arc length
                            #normalized to [0,1]

        if self.use_old: #use previously calculated lines
            line_fn = path.join(self.dir_name, "line_"+self.base_name)
            if path.isfile(line_fn): #if we can
                self.line_points = np.loadtxt(line_fn)
            else: #there is no old line
                self.use_old = False


    def __init__(self, fn, baseline=None, baseline_every=10, use_old=False,
                 use_intensity=True):
        self.filename = fn
        self.dir_name, self.base_name = path.split(fn)
        self.use_old = use_old;

        self.reload()
        self.use_intensity = use_intensity

        if baseline is None:
            self.idx = int(fn.split('_')[-2])
            if self.idx % baseline_every == 0:
                self.is_baseline = True
            else:
                self.is_baseline = False
        else:
            self.is_baseline=baseline

    def bound_box(self, x, y, xexpand=1, yexpand=1):
        xmin = min(x); ymin = min(y)
        xmax = max(x); ymax = max(y)

        mx = 0.5*(xmin + xmax)
        dx = 0.5*(xmax - xmin)
        xmin = mx - xexpand*dx
        xmax = mx + xexpand*dx

        my = 0.5*(ymin + ymax)
        dy = 0.5*(ymax - ymin)
        ymin = my - yexpand*dy
        ymax = my + yexpand*dy

        return np.array([xmin, xmax, ymin, ymax])

    def filter_line(self, per = 0.1, nn = 100, maxiter=10,
                    simple = True):
        bb0 = self.bound_box(self.x0, self.y0)
        x = self.x0
        y = self.y0
        dat = self.dat0

        if simple:
            x0 = np.average(x, weights=dat)
            y0 = np.average(y, weights=dat)
            s  = np.sqrt(np.average((y-y0)**2, weights=dat))
            ix = np.logical_and.reduce(
                [y < y0 + 0.75*s, y > y0 - 0.5*s,
                 x < x0 + 350, x > x0 - 350])
            self.x = x[ix]
            self.y = y[ix]
            self.dat = dat[ix]
            self.filtered=True
            return

        bb = bb0
        L0 = bb[1] - bb[0]

        lock_length = False

        for k in range(maxiter):
            xx = np.column_stack((x, y))
            KDt = spt.cKDTree(xx)
            nnd, nndi = KDt.query(xx, k=np.sqrt(x.size), p=1)
            dst = np.std(nnd, axis=1)
            p = np.percentile(dst, 100*(1-per))
            ix = (dst < p)
            if(lock_length):
                ix = np.logical_or(ix, x > bb[0])
                ix = np.logical_or(ix, x <= bb[1])
                mid = np.mean(y)
                h = 0.5*np.std(y)
                ix = np.logical_or(ix, y < mid - h)
                ix = np.logical_or(ix, y > mid + h)
            if(x[ix].size < 0.5*self.N0):
                #print('filter: too many removed, stop')
                break
            bb1 = self.bound_box(x[ix], y[ix])
            #check the length of the line
            if(bb1[1] - bb1[0] < 0.8*L0 and not lock_length):
                #print('filter: line too short, lockig length')
                lock_length = True
            #check convergence of the bounding box
            if(all(abs(bb - bb1) < 0.05*bb)):
                #print('filter: converged')
                break;
            x = x[ix]; y = y[ix]; bb = bb1
            dat = dat[ix]
        #if k==maxiter-1:
        #    print('filter: maxiter reached')

        #h = np.std(y)*2
        #mid = np.mean(y)
        #ix = np.logical_and(y < mid + h, y > mid - h)
        #x = x[ix]
        #y = y[ix]

        self.x = x
        self.y = y
        self.dat = dat

        #save the size of the clusters for use as weights
        xx = np.column_stack((x, y))
        KDt = spt.cKDTree(xx)
        nnd, nndi = KDt.query(xx, k=np.sqrt(x.size/2), p=1)
        #self.sigmas = np.mean(nnd, axis=1)*0.5
        #self.sigmas = nnd[:,17]
        self.sigmas = np.zeros_like(x)
        for k in range(self.sigmas.size):
            xi = x[nndi[k,1:]]
            yi = y[nndi[k,1:]]
            cx = xi.mean(); cy = yi.mean()
            sx = xi.std(); sy = yi.std()
            #self.sigmas[k] = np.sqrt(np.std(xi)**2 + np.std(yi)**2)
            #self.sigmas[k] = np.sqrt(np.std(xi)**2 + np.std(yi)**2)
            s = np.sqrt(sx**2 + sy**2)
            r = np.sqrt((x[k] - cx)**2 + (y[k] - cy)**2)
            self.sigmas[k] = s*(1.5 + 0.5*np.tanh(r/s - 1))
        self.weights = np.sum(nnd < 5, axis=1)
        self.filtered = True

    def save_img(self, fn=None, close=True, newpic=False):
        if self.use_old and not newpic:
            return

        f, axo = plt.subplots(1,1)


        if self.x is not None and self.y is not None:
            bb = self.bound_box(self.x, self.y)
        else:
            bb = self.bound_box(self.line_points[:,0], self.line_points[:,1],
                                yexpand=5)

#        if hasattr(self, 'pdf_img'):
#            img = axo.imshow(np.rot90(np.reshape(self.pdf_img, self.X.shape)), extent=bb,
#                             aspect='auto')#, vmin=0.5*self.pdf_img.max())
#            f.colorbar(img)
        img = abs(np.log(self.data))
        img[np.isinf(img)] = 0
        img = gaussian_filter(img, 2)
        axo.imshow(img, cmap='Greys_r')

#        axo.scatter(self.x0, self.y0, s=0.25, color='0.6')
#        if(self.x is not None and self.y is not None):
#            axo.scatter(self.x, self.y, s=0.25, color='black')
        if(self.line_xn is not None):
            xl, yl = self.line_points_init.T
#            xl, yl = self.line_xn(np.linspace(0, 1, 256))
            axo.plot(xl, yl, '-o', color='green', lw=0.5,
                     markersize=1)
        if(self.line_xn is not None):
            xl, yl = self.line_points.T
#            xl, yl = self.line_xn(np.linspace(0, 1, 256))
            axo.plot(xl, yl, '-o', color='red', lw=1,
                     markersize=3)

        axo.set_xlim(bb[:2])
        axo.set_ylim(bb[2:])

        f.tight_layout()

        if fn is None:
            fn = "".join([self.dir_name, '/', self.base_name[:-4], '.png'])
            if newpic:
                fn = "".join([self.dir_name, '/new_', self.base_name[:-4], '.png'])
        plt.savefig(fn)
        if(close):
            plt.close(f)
        else:
            return f, axo

    def save_line(self, fn=None):
        if not self.use_old:
            xs, ys = self.line_points.T
            line_out = np.column_stack((xs, ys))
            if(fn is None):
                fn = "".join([self.dir_name, '/line_', self.base_name])
            np.savetxt(fn, line_out)

    def hess(self, x, y): #returns a vector of Hessian matrices
        p_xx = self.spdf(x.ravel(), y.ravel(), dx=2, dy=0, grid=False)
        p_yy = self.spdf(x.ravel(), y.ravel(), dx=0, dy=2, grid=False)
        p_xy = self.spdf(x.ravel(), y.ravel(), dx=1, dy=1, grid=False)

        return np.column_stack((p_xx, p_xy, p_xy, p_yy)).reshape(x.size, 2, 2)

    def move_pt(self, x, y, Dm):
        H = self.hess(x, y)[0]
        w, vv = eigh(H)
        v = vv[:,0]
        #v = np.array((0,1))

        g_x = self.spdf(x, y, dx=1, dy=0, grid=False)
        g_y = self.spdf(x, y, dx=0, dy=1, grid=False)

        B = g_x * v[0] + g_y * v[1]
        C = np.dot(v, np.dot(H, v))

        u = -B/C
        xn, yn = np.array([x,y]) + u*v
        d = abs(u)
        if(d > Dm):
            xnp, ynp = np.array([x,y]) + Dm*v
            xnm, ynm = np.array([x,y]) - Dm*v
            d = Dm

            pp = self.spdf(xnp, ynp, grid=False)
            pm = self.spdf(xnm, ynm, grid=False)
            if(pp > pm):
                xn = xnp; yn = ynp; u = Dm
            else:
                xn = xnm; yn = ynm; u = -Dm

        dP = (self.spdf(xn, yn, grid=False) - self.spdf(x, y, grid=False))
        r  = dP / abs(B*u + 0.5*C*u**2)
        return xn, yn, r, d

    def optim_pt(self, x, y, Dmax=5):
        D = 0.25*Dmax

        for k in range(100):
            xn, yn, r, d = self.move_pt(x, y, D)
            if(r > 0.25):
                #xo = x; yo = y
                x = xn; y = yn

            if(r < 0.1):
                D = 0.5*D
            elif(d > 0.5*D and r > 0.5):
                D = min([2*D, Dmax])

            #if(r > 0.1 and abs(yo - y)/yo < 1e-6 and abs(xo - x)/xo < 1e-6):
            #    break
            H = self.hess(x, y)[0]
            w, vv = eigh(H)
            if(w[0] < 0): #we are near the ridge
                #check the convergence criterium
                gx = self.spdf(x, y, dx=1, dy=0, grid=False)
                gy = self.spdf(x, y, dx=0, dy=1, grid=False)

                n = abs(np.dot(vv[:,0], [gx, gy]))
                #n  = norm(pgrad, 2)
                ng = norm([gx, gy], 2)
                if(n/ng < 1e-6):
                    #print('converged')
                    break
                if(any(np.isnan([n,r,D,d]))):
                    print(x)
                    print('Nan')
                    break
                if(r >= 0 and r < 1e-10):
                    #print('small r')
                    break
                if(d < 1e-10):
                    #print('small d')
                    break
        if(k > 98):
            print('max iter')
        return x, y

    def line_2dif(self, xs, ys): #returns the second arclength derivative
        ps = np.column_stack((xs, ys))
        dx = np.diff(xs)
        dy = np.diff(ys)
        ls = np.append([0], np.cumsum(np.sqrt(dx**2 + dy**2))) #the arc length
        dls = np.sqrt((dx**2 + dy**2))

        ddp = np.zeros_like(ps)
        #the first point
        ddp[0] = 0#2*(ls[1]*ps[2] - ls[2]*ps[1] - ps[0]*(ls[1] - ls[2]))/(ls[1]*ls[2]*(ls[2] - ls[1]))

        #the last point
        ddp[-1] = 0#2*(ls[1]*ps[2] - ls[2]*ps[1] - ps[0]*(ls[1] - ls[2]))/(ls[1]*ls[2]*(ls[2] - ls[1]))

        #the in-between points
        for k in range(1,len(xs)-1):
            ddp[k,:] = 2*(dls[k]*ps[k-1] + dls[k-1]*ps[k+1] - ps[k]*(dls[k] + dls[k-1]))
            ddp[k,:] /= dls[k]*dls[k-1]*(dls[k] + dls[k-1])

        return ddp[:,0], ddp[:,1]

    def optim_pts_el(self, xs, ys, Dmax=5, a=1):
        D = 0.25*Dmax*np.ones_like(xs)
        converged = np.zeros_like(xs, dtype=bool)

        xout = np.copy(xs); yout = np.copy(ys)
        xout0 = np.copy(xs); yout0 = np.copy(ys)

        for k in range(100):
            if all(converged):
                break

            #move all points by 1 step
            for j in np.arange(len(xs))[np.logical_not(converged)]:
                x = xout[j]; y = yout[j]
                xn, yn, r, d = self.move_pt(x, y, D[j])
                if(r > 0.25):
                    #xo = x; yo = y
                    xout[j] = xn; yout[j] = yn

                if(r < 0.1):
                    D[j] = 0.5*D[j]
                elif(d > 0.5*D[j] and r > 0.5):
                    D[j] = min([2*D[j], Dmax])

            #apply elasticity
#            ix = np.argsort(xout)
#            xout = xout[ix]; yout = yout[ix]
            ddx, ddy = self.line_2dif(xout, yout)
            ix = np.logical_not(converged)
            xshift = a*ddx[ix]
            jx = abs(xshift) > Dmax
            if any(jx):
                xshift[jx] = (Dmax*np.sign(xshift))[jx]
            xout[ix] += xshift
            yshift = a*ddy[ix]
            jx = abs(yshift) > Dmax
            if any(jx):
                yshift[jx] = (Dmax*np.sign(yshift))[jx]
            yout[ix] += yshift

            conv = np.logical_and(abs((xout - xout0)/xout) < 0.001,
                                  abs((yout - yout0)/yout) < 0.001)

            converged = np.logical_or(converged, conv)
            xout0 = np.copy(xout)
            yout0 = np.copy(yout)

        if(k == 99):
            print('max iter')

        ix = np.logical_not(np.logical_or(np.isnan(xout), np.isnan(yout)))

        return xout[ix], yout[ix], ix

    def construct_kde(self, bandwidth=None, fancy_sigmas=False,
                      simple=True):
        DATA = np.vstack((self.x, self.y))

        xmin, xmax, ymin, ymax = self.bound_box(self.x,self.y)
        xmin = int(xmin); xmax = int(xmax)
        ymin = int(ymin); ymax = int(ymax)
        Nx = 256
        Ny = 128
        self.X, self.Y = np.mgrid[xmin:xmax:Nx*1j, ymin:ymax:Ny*1j]
        if simple:
            Nx = xmax-xmin
            Ny = ymax-ymin
            self.X, self.Y = np.mgrid[xmin:xmax:Nx*1j, ymin:ymax:Ny*1j]
            pdf = abs(np.log(self.data))
            pdf[np.isinf(pdf)] = 0
            pdf = gaussian_filter(np.rot90(pdf), 5)
            pdf = np.flipud(pdf)
            pdf = pdf[xmin:xmax, ymin:ymax]
            xx = np.linspace(xmin, xmax, Nx)
            yy = np.linspace(ymin, ymax, Ny)
            self.spdf = interp.RectBivariateSpline(xx, yy, pdf, s=0)
            self.pdf_img = self.spdf(self.X.ravel(), self.Y.ravel(), grid=False)
            return

        if bandwidth is None: #calculate the bandwidth
            stds = []
            Nbins = 32
            L = xmax - xmin
            Dx = L / Nbins
            for k in range(Nbins):
                ix = np.logical_and(self.x > xmin + k*Dx,
                                    self.x < xmin + (k+1)*Dx)
                if any(ix):
                    s = self.y[ix].std()
                    if(not np.isnan(s)):
                        stds.append(self.y[ix].std())
            bandwidth = np.median(stds) * 0.5
            print(self.sigmas.mean())

        if fancy_sigmas:
            sig = self.sigmas
        else:
            sig = None

        if self.use_intensity:
            kde = gauss_line_kde(DATA, bandwidth=bandwidth, sigmas=sig,
                                 weights=self.dat)
        else:
            kde = gauss_line_kde(DATA, bandwidth=bandwidth, sigmas=sig)

        pos = np.vstack([self.X.ravel(), self.Y.ravel()])
        pdf = (np.reshape(kde(pos), self.X.shape))

        pdf = np.log(pdf)

        #calculate the gradient field using splines
        xx = np.linspace(xmin, xmax, Nx)
        yy = np.linspace(ymin, ymax, Ny)
        try:
            self.spdf = interp.RectBivariateSpline(xx, yy, pdf, s=0)
        except ValueError as e:
            print('Value error in {}'.format(self.filename))
            print(e)
            raise

        self.pdf_img = self.spdf(self.X.ravel(), self.Y.ravel(), grid=False)


    def build_lines(self, cleanup=False):
        ####create the parametric representations
        removed = 1
        it=0
        if cleanup:
            while removed > 0 and it < 3:
                removed = 0
                lxn = self.line_points
                ix = [False]
        #        print(len(lxn))
                for k in range(1,len(lxn)-1):
                    p0 = lxn[k-1,:]
                    p1 = lxn[k,:]
                    p2 = lxn[k+1,:]

                    d10 = norm(p1 - p0)
                    d20 = norm(p2 - p0)
                    d21 = norm(p2 - p1)
                    c   = np.dot(p1 - p0, p2 - p1)/d10/d21
                    c0  = 0

                    if d20 < 0.25*(d10 + d21):
                        ix.append(False)
                        removed+=1
                    elif d21 > 5*d10:
                        ix.append(False)
                        removed+=1
                    elif d10 > 5*d21:
                        ix.append(False)
                        removed+=1
                    elif c < c0:
                        ix.append(False)
                        removed+=1
                    else:
                        ix.append(True)


                ix.append(False)
                ix = np.array(ix, dtype=bool)
                self.line_points = self.line_points[ix,:]
                if self.line_points_init is not None:
                    self.line_points_init = self.line_points_init[ix,:]

                it+=1

        lxn = self.line_points
#        print(lxn.shape)

        ##in x
#        print(removed)
        ix = np.argsort(lxn[:,0])
        line = interp.interp1d(lxn[ix,0], lxn[ix,1], kind='linear', bounds_error=False,
                               copy=True)

        if(self.is_baseline): #we know that the correct solution is a straight line
            polyp = np.polyfit(lxn[ix,0], lxn[ix,1], 1)
            line = lambda x: np.polyval(polyp, x)

        self.line_x = lambda x: line(x)

        #the normalized version
        Lx = lxn[:,0].max() - lxn[:,0].min()
        Lx0 = lxn[:,0].min()
        self.line_xn = lambda xn: np.vstack((Lx0 + xn*Lx,
                                            self.line_x(Lx0 + xn*Lx)))

        ##in arc length
        arclen = np.append([0], np.sqrt(np.cumsum(np.diff(lxn[:,0])**2 +
                                                  np.diff(lxn[:,1])**2)))
        L = arclen.max();
        xs_l = interp.interp1d(arclen, lxn[:,0], kind='linear', bounds_error=False,
                              copy=True)
        ys_l = interp.interp1d(arclen, lxn[:,1], kind='linear', bounds_error=False,
                              copy=True)
        self.line_l  = lambda l: np.array((xs_l(l), ys_l(l)))
        self.line_nl = lambda nl: self.line_l(nl*L)

    def find_ridge(self, kde_bw = None, Ninit=256, cleanup=False,
                   fancy_sigmas=False, elastic=False,
                   simple=False):
        if self.use_old:
            self.build_lines(cleanup=cleanup)
            return

        if not self.filtered:
            self.filter_line()
        bb = self.bound_box(self.x, self.y)
        xmin, xmax, ymin, ymax = bb
        xmin = int(xmin); xmax = int(xmax)
        ymin = int(ymin); ymax = int(ymax)
        N = Ninit
        lx = np.empty([0,2])
        Dx = (xmax - xmin)/N
        W = 32
        le = xmin

        if not self.is_baseline:
            self.construct_kde(bandwidth=kde_bw, fancy_sigmas=fancy_sigmas)
        img = sparse.coo_matrix((self.dat, (self.y, self.x)), shape=(1024, 1024)).todense()
#        xix = np.arange(self.x.min()-1, self.x.max()-1)
#        print(img.shape, self.x.min(), self.x.max(), self.x.max() - self.x.min())
        xix = np.arange(1024)
        gd = gaussian_filter(img, 10)
        yprof_global = np.sum(gd, axis=1)
        MM = yprof_global.max()
        yix = yprof_global > MM/2
        LL = np.arange(len(yprof_global))[yix].min()
        gd = gd[yix,:]
#        print("LL = {}".format(LL))
#        f, ax = plt.subplots(1,1)
#        ax.plot(yprof_global)
#        f, ax = plt.subplots(1,1)
#        ax.imshow(img)
        for n in range(N):
#            ix = np.logical_and(self.x > le - W/2,
#                                self.x < le + W/2)
            ix = np.logical_and(xix > le - W/2,
                                xix < le + W/2)
            le = le + Dx
#            if not self.is_baseline:
#                w = self.spdf(self.x[ix], self.y[ix], grid=False)
#            else:
#                w = np.ones_like(self.y[ix])
            yprof = np.sum(gd[:,ix], axis=1)
            thr = np.percentile(yprof, 90)
            cy = np.median(np.arange(len(yprof))[yprof>thr]) + LL
            cx = le
#            cy = np.average(self.y[ix], weights=w)
#            cx = np.average(self.x[ix], weights=w)
            p = [cx, cy]
            lx = np.vstack((lx, p))

        if self.is_baseline:
            p = np.polyfit(lx[:,0], lx[:,1], deg=1)
            xmin = lx[:,0].min()
            xmax = lx[:,0].max()
            xs = np.linspace(xmin, xmax, Ninit)
            ys = p[1] + p[0]*xs
            lxn = np.column_stack((xs, ys))
            self.line_points_init = lx
            self.line_points      = lxn

            self.build_lines()
            return

        if simple:
            self.line_points_init = lx
            self.line_points      = lx
            self.build_lines(cleanup)
            return

        #optimization
        if elastic:
            xout, yout, ix = self.optim_pts_el(lx[:,0], lx[:,1])
            self.line_points = np.column_stack((xout, yout))
            self.line_points_init = lx[ix]
        else:
            self.line_points_init = []
            lxn = np.empty([0,2])
            for p in lx:
                if(self.spdf(p[0], p[1], grid=False) > 0.1*self.pdf_img.max()):
                    #print("strong enough")
                    x, y = self.optim_pt(p[0], p[1])
                    if(np.sqrt((p[0] - x)**2 + (p[1] - y)**2) < 200):
                        #print("close enough")
                        self.line_points_init.append(p)
                        lxn = np.vstack((lxn, [x, y]))

            cx = np.median(lxn[:,0])
            ix = np.logical_and(lxn[:,0] > cx - 355,
                                lxn[:,0] < cx + 355)
            lxn = lxn[ix,:]

            #save the initial guess and the optimized line
            self.line_points_init = np.array(self.line_points_init)[ix,:]

            #sort
            ix = np.argsort(lxn[:,0])
            self.line_points = lxn[ix,:]
            self.line_points_init = self.line_points_init[ix,:]

        ####create the parametric representations
#        print("Points: {}".format(len(self.line_points)))
        self.build_lines(cleanup=cleanup)

    def local_kde_line(self, window=16, shift=4):
        if not self.filtered:
            print('filtering')
            self.filter_line()

        xmin, xmax, ymin, ymax = self.bound_box(self.x, self.y)
        lwe = xmin #left window edge
        rwe = lwe + window #right window edge
        line_points = []
        line_points_simple = []
        while(rwe < xmax):
            ix = np.logical_and(self.x >= lwe,
                                self.x <= rwe)
            if sum(ix) > 1:
                xx = self.x[ix]
                yy = self.y[ix]
                #print(xx)
                #print(yy)
                if any(np.isnan(xx)) or any(np.isnan(yy)):
                    print('nan in  {} {}'.format(rwe, lwe))

                dataset = np.vstack((xx, yy))
                #kde = gaussian_kde(dataset)
                kde = gauss_line_kde(dataset, bandwidth=10)
                xl, xh, yl, yh = self.bound_box(xx, yy)
                xg = np.linspace(xl, xh, 64)
                yg = np.linspace(yl, yh, 64)
                xxg, yyg = np.meshgrid(xg, yg)
                pdf = kde(np.vstack((xxg.ravel(), yyg.ravel())))
                ix = np.argmax(pdf)
                x0 = xxg.ravel()[ix]
                y0 = yyg.ravel()[ix]
#                pdf = np.reshape(pdf, xxg.shape)
#                pdf_s = interp.RectBivariateSpline(xg, yg, pdf, s=0)
#                f = lambda p: -pdf_s(p[0], p[1], grid=False)
                f = lambda p: -kde(p)
                res = opt.minimize(f, (x0, y0))
                line_points.append(res.x)
                if not res.success:
                    print('failed!')
#                line_points.append((x0, y0))
                line_points_simple.append((xx.mean(), yy.mean()))


            lwe = lwe + min((shift, window))
            rwe = rwe + min((shift, window))

        self.lkde_line_points = np.array(line_points)
        return self.lkde_line_points, np.array(line_points_simple)


if __name__ == "__main__":
    plt.close('all')
#    import cProfile
    filename = path.join(data_dirs.get_base_data_dir(which='data'),
                         'T165/50mms/images/1000ms_1/'
                         'LHe_1_65K_280mW_1000msdecay_10msdrift_50mms_'
                         '35fspulses_6imgpulses_14msecexp_195.dat')
    tl = TracerLine(filename,
                    use_old=False, baseline=False)
#    tl = TracerLine('./test_img_1_1.dat', use_old = False, baseline=False)
#    ll = np.loadtxt("test_line_1.dat")
#    cProfile.run('tl.find_ridge()')
    tl.find_ridge(cleanup=True)
    tl.save_img(newpic=True)
    tl.save_line()
    ls= tl.line_points_init
    #l, ls = tl.local_kde_line(window=64, shift=4)
    print(tl.N0)
    print(tl.x.size)
#    d = np.loadtxt('Image_6.txt')
    f, axo = tl.save_img(close=False)
#    axo.plot(d[:,0], d[:,1], lw=2)
    axo.plot(tl.line_points_init[:,0],
             tl.line_points_init[:,1], '-o', color='green')
#    for (p0, p) in zip(tl.line_points_init, tl.line_points):
#        axo.plot([p0[0], p[0]], [p0[1], p[1]], color='0.8')
#    axo.plot(ll[:,0], ll[:,1], lw=3, color='blue')
    #axo.plot(l[:,0], l[:,1], '-o', color='magenta', lw=3)
    #axo.plot(ls[:,0], ls[:,1], 'o', color='magenta', markersize=6)
    tl.save_line()

    f, ax = plt.subplots(1,1)
    ax.scatter(tl.x, tl.y, s=1)
    ax.plot(tl.line_points[:,0], tl.line_points[:,1], lw=2, color='red')
    ax.plot(tl.line_points_init[:,0], tl.line_points_init[:,1], lw=2, color='green')