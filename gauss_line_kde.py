# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

# Standard library imports.
import warnings

# Scipy imports.
from scipy._lib.six import callable, string_types
from scipy import linalg, special

from numpy import atleast_2d, reshape, zeros, newaxis, dot, exp, pi, sqrt, \
     ravel, power, atleast_1d, squeeze, sum, transpose
import numpy as np
from numpy.random import randint, multivariate_normal

__all__ = ['gauss_line_kde']


class gauss_line_kde(object):
    def __init__(self, dataset, bandwidth, weights=None, sigmas=None):
        self.dataset = atleast_2d(dataset)
        if not self.dataset.size > 1:
            raise ValueError("`dataset` input should have multiple elements.")

        self.d, self.n = self.dataset.shape
        self.bandwidth = bandwidth
        if weights is not None:
            self.weights = weights
        else:
            self.weights = np.ones(self.n)
        self.sigmas = sigmas

    def evaluate(self, points):
        points = atleast_2d(points)

        d, m = points.shape
        if d != self.d:
            if d == 1 and m == self.d:
                # points was passed in as a row vector
                points = reshape(points, (self.d, 1))
                m = 1
            else:
                msg = "points have dimension %s, dataset has dimension %s" % (d, self.d)
                raise ValueError(msg)

        result = zeros((m,), dtype=float)
        
        h = self.bandwidth*np.ones(self.n)
        if(self.sigmas is not None):
            h = self.sigmas

        if m >= self.n:
            # there are more points than data, so loop over data
            for i in range(self.n):
                diff = self.dataset[:, i, newaxis] - points
                energy = sum(diff*diff/h[i]**2,axis=0) / 2.0
                result = result + exp(-energy)/h[i]*self.weights[i]
        else:
            # loop over points
            for i in range(m):
                diff = self.dataset - points[:, i, newaxis]
                energy = sum(diff * diff/h**2, axis=0) / 2.0
                result[i] = sum(exp(-energy)*self.weights/h, axis=0)

        return result

    __call__ = evaluate

    def pdf(self, x):
        """
        Evaluate the estimated pdf on a provided set of points.
        Notes
        -----
        This is an alias for `gaussian_kde.evaluate`.  See the ``evaluate``
        docstring for more details.
        """
        return self.evaluate(x)

    def logpdf(self, x):
        """
        Evaluate the log of the estimated pdf on a provided set of points.
        Notes
        -----
        See `gaussian_kde.evaluate` for more details; this method simply
        returns ``np.log(gaussian_kde.evaluate(x))``.
        """
        return np.log(self.evaluate(x))